#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 22-March-2019

# This script masters the insertion of the RD53 component data.

from AnsiColor  import Fore, Back, Style
from Exceptions import *
from DataReader import *
from Utils      import *
from optparse   import OptionParser
from RD53       import *


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <path to RD53 data>, ...", version="1.1")

   p.add_option( '-v','--ver',
                  type    = 'string',
                  default = 'RD53A',
                  dest    = 'version',
                  metavar = 'STR',
                  help    = 'Input the version of the CBC production')

   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')

   if opt.verbose:  BaseUploader.verbose = True

   for p in args: rd53_files = search_files(p,'*.txt')

   #rd53_datae = TableReader(rd53_files[0],csv_delimiter=':')
   #data = rd53_datae.getDataAsCWiseDict()
   #print rd53_datae.getDataAsCWiseDictRowSplit()[0:1]

   wafer_conf = {
      'kind_of_part'    : 'RD53 Wafer',
   }
   wafer_conf['version']     = opt.version

   chip_conf = {
      'kind_of_part'    : 'RD53 Chip',
   }
   chip_conf['version']     = opt.version

   data_conf = {
      'kind_of_part'    : 'RD53 Chip',
      'run_type'        : 'Probing process',
      'run_number'      : 1,
      'table_name'      : 'CONF_RD53_PRB',
      'data_name'       : 'RD53 Chip Configuration From Probing',
      'dataset_version' : '1.0',
      'DBvar_vs_TxtHeader' : {  'IREF_MUAMP'       : 'Iref',
                                'IREF_TRIM'        : 'IrefTrim',
                                'VREF_A_V'         : 'Vref_A',
                                'VREF_A_TRIM'      : 'Vref_A_Trim',
                                'VREF_D_V'         : 'Vref_D',
                                'VREF_D_TRIM'      : 'Vref_D_Trim',
                                'VREF_ADC_V'       : 'Vref_ADC',
                                'VREF_ADC_TRIM'    : 'Vref_ADC_Trim',
                                'MON_ADC_TRIM'     : 'MON_ADC_Trim',
                                'VCAL_HIGH_SLOPE'  : 'VCAL_HIGH_slope',
                                'VCAL_HIGH_OFFSET' : 'VCAL_HIGH_offset',
                                'VCAL_MED_SLOPE'   : 'VCAL_MED_slope',
                                'VCAL_MED_OFFSET'  : 'VCAL_MED_offset',
                                'TREFTSENSOR_DEGC' : 'TrefTsensors',
                                'NF1'              : 'Nf1',
                                'NF2'              : 'Nf2',
                                'NF3'              : 'Nf3',
                                'NF4'              : 'Nf4'
                             }
   }

   grading_conf = {
      'kind_of_part'    : 'RD53 Chip',
      'run_type'        : 'Probing process',
      'run_number'      : 1,
      'table_name'      : 'PROC_GRADING',
      'data_name'       : 'Tracker Component Grading',
      'dataset_version' : '1.0',
      'DBvar_vs_TxtHeader' : {  'GRADING_RANK'     : 'Grading',
                                'GRADING_VALUE'    : 'Color',
                                'VERSION_OF_CUTS'  : 'CutVersion',
                             }
   }


   run_number=1
   for f in rd53_files:
      runp = RunProvider(run_number, increment_run=False, use_database=False)

      wafer_tag     = f.split('_')[2]
      condData_name = '{}_RD53configuration.xml'.format(wafer_tag)
      gradData_name = '{}_RD53grading.xml'.format(wafer_tag)

      rd53_data = TableReader(f,csv_delimiter=':')
      rd53_wafer = RD53WaferFromTxtDatabase(wafer_tag,wafer_conf,rd53_data)
      rd53_wafer.dump_xml_data()

      rd53_chip = RD53ChipFromTxtDatabase(wafer_tag,chip_conf,rd53_data)
      rd53_chip.dump_xml_data(chip_wafer=rd53_wafer)

      rd53_grad = RD53GradingDataFromTxt(wafer_tag,grading_conf,rd53_data,runp)
      rd53_grad.dump_xml_data(filename=gradData_name)

      rd53_cond = RD53ProbingDataFromTxt(wafer_tag,data_conf,rd53_data,runp)
      rd53_cond.dump_xml_data(filename=condData_name)

      run_number += 1
