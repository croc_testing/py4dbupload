#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 06-June-2020

# This script masters the insertion of the Sensor PreSeries data.

import os,traceback,sys

from AnsiColor  import Fore, Back, Style
from DataReader import TableReader
from Sensor     import *
from Utils      import *
from optparse   import OptionParser


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <serial number file>, ...", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the excel files with measurement data.')

   p.add_option( '-a','--attributes',
               action  = 'append',
               default = "[(\'Sensor Type\',\'PreSeries\')]",
               dest    = 'attributes',
               metavar = 'STR',
               help    = 'List of tuple with attribute spec. as (type,value)')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')


   # get the CERN sso cocky required for connecting to the database
#   try:
#       import cern_sso_api
#   except:
#       print( 'Cannot import cern_sso_api: resthub package not in the path.' )
#       print( 'Download the resthub package and run bin/setup.sh' )
#       sys.exit(1)

#   try:
#      dburl = 'https://cmsdca.cern.ch/trk_rhapi'
#      cern_sso_api.cern_api( dburl )
#   except:
      # try to remove the previous .session file and login again
#      try:
#         os.remove('.session.cache')
#         cern_sso_api.cern_api( dburl )
#      except Exception:
#         print("Exception while getting CERN sso cookie:")
#         print("-"*60)
#         traceback.print_exc(file=sys.stdout)
#         print("-"*60)
#         sys.exit(1)


   if opt.verbose:  BaseUploader.verbose = True

   # gather excel files conatining the data measurements
   excel_data_files = search_files(opt.data_path,'*.xlsx')

   # write the configuration dictionary for the Wafer components
   wafer_conf = {
      'kind_of_part'     : 'PS-s Wafer',
      'manufacturer'     : 'Hamamatsu',
      'unique_location'  : 'Vienna',
      'description'      : 'PS-s Wafer from Pre Series production.',
      'attributes'       : [('Sensor Type','PreSeries'),('Status','Good')],
   }

   # write the configuration dictionary for the Sensor components
   sensor_conf = {
      'kind_of_part'     : 'PS-s Sensor',
      'manufacturer'     : 'Hamamatsu',
      'unique_location'  : 'Vienna',
      'description'      : 'PS-s Sensor from Pre Series production.',
      'attributes'       : [('Sensor Type','PreSeries'),('Status','Good')],
   }

   # write the configuration dictionary for the IV data
   IV_conf = {
      'kind_of_part'     : 'PS-s Sensor',
      'run_type'         : 'VQC',
      'run_location'     : 'Hamamatsu',
      #'run_operator'     : 'None',
      'run_comment'      : 'Some errors in the data table.',
      'run_date'         : '2020-07-31',
      'description'      : 'IV measurements done at Hamamatsu.',
      'version'          : 'v1',
      'table_name'       : 'TEST_SENSOR_IV',
      'data_name'        : 'Tracker Strip-Sensor IV Test',
      'var1_name'        : 'VOLTS',
      'var2_name'        : 'CURRNT_AMP',
      'var3_name'        : 'TEMP_DEGC',
      'var4_name'        : 'RH_PRCNT',
   }

   # write the configuration dictionary for the CV data
   CV_conf = {
      'kind_of_part'     : 'PS-s Sensor',
      'run_type'         : 'VQC',
      'run_location'     : 'Hamamatsu',
      #'run_operator'     : 'None',
      'run_comment'      : 'Some errors in the data table.',
      'run_date'         : '2020-07-31',
      'description'      : 'CV measurements done at Hamamatsu.',
      'version'          : 'v1',
      'table_name'       : 'TEST_SENSOR_CV',
      'data_name'        : 'Tracker Strip-Sensor CV Test',
      'var1_name'        : 'VOLTS',
      'var2_name'        : 'CAP_PFRD',
      'var3_name'        : 'TEMP_DEGC',
      'var4_name'        : 'RH_PRCNT',
   }

   #db = DBaccess(verbose=opt.verbose)

   runp1 = RunProvider(1, use_database=False)  # set to True for extracting run from
                                               # CMSR Database
   runp2 = RunProvider(1)   # extract runs from internal counter starting at 1

   for sensor_sn_file in args:
      listname = os.path.splitext(os.path.basename(sensor_sn_file))[0]
      sn_list = TableReader(sensor_sn_file)
      wf_list = TableReader(sensor_sn_file)
      wafer = WaferFromIDlist('{}'.format(listname),wafer_conf,wf_list)
      wafer.dump_xml_data()

      sensor = SensorFromIiDlist('{}'.format(listname),sensor_conf,sn_list)
      sensor.dump_xml_data(wafers=wafer)

      #select corresponding excel file storing the condition data
      datasheet_file = ''
      for f in excel_data_files:
         if os.path.splitext(os.path.basename(f))[0]==listname:
            datasheet_file = f
            break

      mainA_IV = TableReader(datasheet_file,'MAINA IV',m_rows=53)
      mainA_CV = TableReader(datasheet_file,'MAINA CV',m_rows=43)
      mainB_IV = TableReader(datasheet_file,'MAINB IV',m_rows=53)
      mainB_CV = TableReader(datasheet_file,'MAINB CV',m_rows=43)

      IVa = SensorTest('{}_IVmainA'.format(listname),IV_conf,mainA_IV,runp1)
      CVa = SensorTest('{}_CVmainA'.format(listname),CV_conf,mainA_CV,runp2)
      IVb = SensorTest('{}_IVmainB'.format(listname),IV_conf,mainB_IV,runp1)
      CVb = SensorTest('{}_CVmainB'.format(listname),CV_conf,mainB_CV,runp2)

      rl1 = IVa.dump_xml_data(sensor)
      rl2 = CVa.dump_xml_data(sensor,rl1)
      rl3 = IVb.dump_xml_data(sensor)
      rl4 = CVb.dump_xml_data(sensor,rl3)
