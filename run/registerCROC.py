#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 12-Oct-2021

# This script masters the registration of the CROC Chips and Wafers components.


import os,traceback,sys,time
import numpy as np
import pandas as pd

from AnsiColor   import Fore, Back, Style
from DataReader  import TableReader,scale,scale2
from CROC        import *
from Utils       import *
from datetime    import date
from optparse    import OptionParser
from decimal     import Decimal, ROUND_HALF_UP
from progressbar import *


def _get_chip_ids(wafermap):
   """Returns the list of chip IDs from a wafermap defined in a pandas DataFrame."""

   # getting the flattened list of (raw) chip IDs from the DataFrame
   chip_ids_raw = wafermap.to_numpy().flatten()

   # filling the list of chip IDs
   chip_ids = [x.replace('-', '') for x in chip_ids_raw if x != '-']
   return chip_ids


# wafermap for the prototype chip
WaferMapProto = pd.DataFrame(
   {
      'Col 0' : [ '-' , '-' , '-' , '-' ,'8-1','7-1','6-1', '-' , '-' , '-' , '-' , '-' ],
      'Col 1' : [ '-' , '-' ,'A-2','9-2','8-2','7-2','6-2','5-2','4-2', '-' , '-' , '-' ],
      'Col 2' : [ '-' ,'B-3','A-3','9-3','8-3','7-3','6-3','5-3','4-3','3-3', '-' , '-' ],
      'Col 3' : [ '-' ,'B-4','A-4','9-4','8-4','7-4','6-4','5-4','4-4','3-4','2-4', '-' ],
      'Col 4' : ['C-5','B-5','A-5','9-5','8-5','7-5','6-5','5-5','4-5','3-5','2-5', '-' ],
      'Col 5' : ['C-6','B-6','A-6','9-6','8-6','7-6','6-6','5-6','4-6','3-6','2-6', '-' ],
      'Col 6' : ['C-7','B-7','A-7','9-7','8-7','7-7','6-7','5-7','4-7','3-7','2-7','1-7'],
      'Col 7' : ['C-8','B-8','A-8','9-8','8-8','7-8','6-8','5-8','4-8','3-8','2-8','1-8'],
      'Col 8' : ['C-9','B-9','A-9','9-9','8-9','7-9','6-9','5-9','4-9','3-9','2-9','1-9'],
      'Col 9' : ['C-A','B-A','A-A','9-A','8-A','7-A','6-A','5-A','4-A','3-A','2-A', '-' ],
      'Col 10': ['C-B','B-B','A-B','9-B','8-B','7-B','6-B','5-B','4-B','3-B','2-B', '-' ],
      'Col 11': [ '-' ,'B-C','A-C','9-C','8-C','7-C','6-C','5-C','4-C','3-C','2-C', '-' ],
      'Col 12': [ '-' ,'B-D','A-D','9-D','8-D','7-D','6-D','5-D','4-D','3-D', '-' , '-' ],
      'Col 13': [ '-' , '-' ,'A-E','9-E','8-E','7-E','6-E','5-E','4-E', '-' , '-' , '-' ],
      'Col 14': [ '-' , '-' , '-' , '-' ,'8-F','7-F','6-F', '-' , '-' , '-' , '-' , '-' ],
   },
   index=['Row 11','Row 10','Row 9','Row 8','Row 7','Row 6','Row 5','Row 4','Row 3','Row 2','Row 1','Row 0']
)

# wafermap for the production chip
WaferMapProd = pd.DataFrame(
   {
      'Col 0' : [ '-' , '-' , '-' , '-' , '-' , '-' ,'1-9','1-8','1-7', '-' , '-' , '-' , '-' , '-' , '-' ],
      'Col 1' : [ '-' , '-' , '-' , '-' ,'2-B','2-A','2-9','2-8','2-7','2-6','2-5','2-4', '-' , '-' , '-' ],
      'Col 2' : [ '-' , '-' ,'3-D','3-C','3-B','3-A','3-9','3-8','3-7','3-6','3-5','3-4','3-3', '-' , '-' ],
      'Col 3' : [ '-' ,'4-E','4-D','4-C','4-B','4-A','4-9','4-8','4-7','4-6','4-5','4-4','4-3','4-2', '-' ],
      'Col 4' : [ '-' ,'5-E','5-D','5-C','5-B','5-A','5-9','5-8','5-7','5-6','5-5','5-4','5-3','5-2', '-' ],
      'Col 5' : ['6-F','6-E','6-D','6-C','6-B','6-A','6-9','6-8','6-7','6-6','6-5','6-4','6-3','6-2','6-1'],
      'Col 6' : ['7-F','7-E','7-D','7-C','7-B','7-A','7-9','7-8','7-7','7-6','7-5','7-4','7-3','7-2','7-1'],
      'Col 7' : [ '-' ,'8-E','8-D','8-C','8-B','8-A','8-9','8-8','8-7','8-6','8-5','8-4','8-3','8-2','8-1'],
      'Col 8' : [ '-' ,'9-E','9-D','9-C','9-B','9-A','9-9','9-8','9-7','9-6','9-5','9-4','9-3','9-2', '-' ],
      'Col 9' : [ '-' , '-' ,'A-D','A-C','A-B','A-A','A-9','A-8','A-7','A-6','A-5','A-4','A-3','A-2', '-' ],
      'Col 10': [ '-' , '-' ,'B-D','B-C','B-B','B-A','B-9','B-8','B-7','B-6','B-5','B-4','B-3', '-' , '-' ],
      'Col 11': [ '-' , '-' , '-' , '-' ,'C-B','C-A','C-9','C-8','C-7','C-6','C-5', '-' , '-' , '-' , '-' ],
      'Col 12': [ '-' , '-' , '-' , '-' , '-' , '-' , '-' ,'D-8', '-' , '-' , '-' , '-' , '-' , '-' , '-' ],
   },
   index=['Row 14','Row 13','Row 12','Row 11','Row 10','Row 9','Row 8','Row 7','Row 6','Row 5','Row 4','Row 3','Row 2','Row 1','Row 0']
)

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] [cvs table file]", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the csv tables with CROC wafer data.')

   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component productioni; overwitten by the date in the cvs table.')

   p.add_option( '-v','--ver',
               type    = 'string',
               default = '2.0',
               dest    = 'ver',
               metavar = 'STR',
               help    = 'Version type of the components; overwitten by the tag in the csv table.')

   p.add_option( '-o','--operator',
               type    = 'string',
               default = '',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'The operator that performed the data registration')

   p.add_option( '--no_chip',
               action  = 'store_true',
               default = False,
               dest    = 'no_chip',
               help    = 'Do not produce chip data.')
   
   p.add_option( '--dummyWafer',
               action  = 'store_true',
               default = False,
               dest    = 'dummyW',
               help    = 'Register dummy wafer.')

   p.add_option( '--test',
               action  = 'store_true',
               default = False,
               dest    = 'test',
               help    = 'Output only the first wafer for test.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--update',
               action  = 'store_true',
               default = False,
               dest    = 'update',
               help    = 'Force the upload of Wafer in update mode')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   


   (opt, args) = p.parse_args()


   if len(args)>1:
      p.error("accepts at most 1 argument!")

   upload_mode = 'update' if opt.update else 'insert'
   
   if opt.dummyW:
      opt.no_chip=True
   
   
   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug

   
   date = opt.date if opt.date!='' else date.today()   
   

   # write the description and the attributes for all the Wafer components
   wafer_conf = {
      'version'          : opt.ver, # parsed from CSV
      'product_date'     : datetime.strftime(date,'%Y-%m-%d'),
      'description'      : '', # set later depending on wafer type
      'attributes'       : [('Status','Good')]
   }

   # write the description and the attributes for all the Sensor components
   chip_conf = {
      'version'          : opt.ver, # parsed from CSV
      'product_date'     : datetime.strftime(date,'%Y-%m-%d'),
      'description'      : '', # set later depending on wafer type
      'attributes'       : [('Status','Good')]
   }

   wafers = UploaderContainer('CROCwafers')
   chips  = UploaderContainer('CROCchips')
   extras = []

   # lists of chip IDs for the different CROC versions (v1 and v2)
   chip_ids_prod = _get_chip_ids(WaferMapProd)
   chip_ids_proto = _get_chip_ids(WaferMapProto)

   # gather croc csv data file
   croc_files = []
   if len(args)==1:  croc_files.append( args[0] )
   else:             croc_files = sorted( search_files(opt.data_path,'*.csv') )

   #prcess csv data file
   for file in croc_files:
      croc_data = TableReader(file,csv_delimiter=';',tabSize=14)
      print ('\n\nWafer to upload',croc_data)
      data = croc_data.getDataAsCWiseDictRowSplit()

      nCROC = len(data)
      bar = progressbar(range(nCROC), widgets=[f'Processing {croc_data.filename}: ', \
             Bar('=', '[', ']'), ' ', Percentage()])

      for i in bar:
         w = data[i]
      #for i,w in enumerate(data):
         # update configuration with data in the csv file
         try:
            batch_id = w['BatchID']
            wafer_id = w['WaferID']
            pd = w['ProductionDate']
            vt = w['VersionTag']
            wafer_conf['product_date']=datetime.strptime(pd, '%d.%m.%Y').strftime('%Y-%m-%d')
            wafer_conf['version']=vt
            chip_conf['product_date']=datetime.strptime(pd, '%d.%m.%Y').strftime('%Y-%m-%d')
            chip_conf['version']=vt
         except:   pass

         # setting description depending on ASIC version (wafer + chips)
         if vt in CROC_V1:
             wafer_conf['description'] = 'CROC Prototype pre-production.'
             chip_conf['description'] = 'CROC Prototype pre-production.'
         elif vt in CROC_V2:
             wafer_conf['description'] = 'CROC production wafer.'
             chip_conf['description'] = 'CROC production chip.'
         else:
             raise ValueError(f'Unrecognised CROC version: "{vt}"!')
         print(f'\n\n{batch_id}_{wafer_id}: {wafer_conf["description"]}',)

         # store wafer classes for registration
         wafer = CROCDummyWafer(wafer_conf,upload_mode,w['BatchID'],w['WaferID']) if opt.dummyW else \
                 CROCwafer(wafer_conf,upload_mode,w['BatchN'],w['BatchID'],w['WaferID'])
         
         #retrieve the wafer description files
         repo = '.' if opt.data_path=='' else opt.data_path
         description_files=search_files(repo,f'*{wafer.name_label()}*')
         wafer.add_description(description_files)
         wafers.add(wafer)
         extras.extend(description_files)

         # using the version tag to select the correct chip IDs
         if vt in CROC_V2:
            chip_ids = chip_ids_prod
         elif vt in CROC_V1:
            chip_ids = chip_ids_proto
         else:
            raise ValueError(f'Invalid CROC version: "{vt}"!')
         print(f'Chips per wafer: {len(chip_ids)}')
         print(f'Chip IDs: {chip_ids}')

         # store chip classes for registration
         if not opt.no_chip:
            for ch_id in chip_ids:
               chip = CROCchip(chip_conf,wafer.name,w['BatchN'],w['WaferID'],ch_id)
               chips.add(chip)

         #bar.update(i)
         time.sleep(0.1)
         if opt.test:  break

   print('\n')
   streams.flush()
   
   files = []
   filename = None
   
   if len(extras)!=0:
      # description files exists, store wafer files one by one
      files = wafers.dump_single_xml_data()
      for f in files:
         fname = os.path.splitext(f)[0]
         from zipfile import ZipFile
         
         zipfilename = f'{fname}.zip'
         with ZipFile(zipfilename, 'w') as (zip):
            zip.write(f)
            os.remove(f)
            for e in extras: 
               efilename = os.path.basename(e)
               if fname in efilename: 
                  zip.write(efilename)
                  os.remove(efilename)
   else:
      filename = wafers.dump_xml_data()
      
   if not opt.no_chip:  chips.dump_xml_data(wafers.uploaders)
