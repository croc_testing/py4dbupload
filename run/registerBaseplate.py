#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 27-May-2022

# This script masters the Baseplate registration. It is normally invoked with 
# the name of a csv file that contains the serial ID of the component to be 
# regstered. The csv file 
# must contains one column only with an header name that specify the type of ID:
# 'serial' or 'barcode' or 'name_label'. The ID of components can also be inserted
# via the --id option thus avoiding to create the csv file; in such a case the 
# type of ID must be specified with the --idtype option.

import os,sys,fnmatch
import pandas as pd

from AnsiColor  import Fore, Style
from Exceptions import *
from Baseplate  import BaseplateComponentT, BaseUploader
from Utils      import UploaderContainer, DBupload
from optparse   import OptionParser


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] [csv file name]", version="1.1")

   p.add_option( '--date',
                  type    = 'string',
                  default = '2022-05-29',
                  dest    = 'date',
                  metavar = 'STR',
                  help    = 'Production date')
   
   p.add_option( '-i','--inserter',
                  type    = 'string',
                  default = None,
                  dest    = 'inserter',
                  metavar = 'STR',
                  help    = 'Override the account name of the operator that performs the data update.')
   
   p.add_option( '--dev',
                  action  = 'store_true',
                  default = False,
                  dest    = 'isDevelopment',
                  help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
                  action  = 'store_true',
                  default = False,
                  dest    = 'upload',
                  help    = 'Perform the data upload in database')
   
   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
                  action  = 'store_true',
                  default = False,
                  dest    = 'debug',
                  help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
 
   


   (opt, args) = p.parse_args()

   if len(args)!=1:
      p.error('Accepts only one argument!')

   # Check date format
   import datetime
   try:
      datetime.datetime.strptime(opt.date, '%Y-%m-%d')
   except ValueError:
      print( Fore.BLUE+f'Incorrect data format ({opt.date}) , should be YYYY-MM-DD')
      print(Fore.BLUE+'exiting ....'+Style.RESET_ALL)
      sys.exit(1)

   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug

   # Getting the part data from database
   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'


   # Load serial numbers from csv file
   df = pd.read_csv(args[0],header=None)

   elements = UploaderContainer('baseplates',opt.inserter)
   
   manufacturer = 'WatAJet'
   location     = 'CERN'

   if len(df.columns)==2:
      # Files describing basepaltes
      for r in df.index:
         panel_id = df.iloc[r][0]
         plate_nr = df.iloc[r][1]
         added = False
         for plate_id in range(1,plate_nr+1):
            plate = BaseplateComponentT(f'{panel_id}_{plate_id}',location,manufacturer,opt.date,'This is a comment for the baseplate.')
            plate.load_attribute('Status','Good')
            for el in elements.uploaders:  
               if el.serial == panel_id:  
                  el.add_children(plate)
                  added = True
            if not added:
               panel = BaseplateComponentT(panel_id,location,manufacturer,opt.date,'This is a comment for the panel.')
               panel.load_attribute('Status','Good')
               panel.add_children(plate)
               elements.add(panel)
            
   elif len(df.columns)==1:
      # files describing panels
      for r in df.index:
         panel_id = df.iloc[r][0]
         panel = BaseplateComponentT(panel_id,location,manufacturer,opt.date,'This is a comment for the panel.')
         panel.load_attribute('Status','Good')
         elements.add(panel)
      
   elements.dump_xml_data()

   
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=opt.debug)
   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      db.upload_data('baseplates.xml',not opt.upload)