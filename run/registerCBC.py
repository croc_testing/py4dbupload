#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 01-January-2023

# This script masters the insertion of the CBC component data.

#import os,sys,fnmatch

#from AnsiColor  import Fore, Back, Style
#from Exceptions import *
from Utils        import search_files,join_data_files,DBupload
from BaseUploader import BaseUploader
from CBC          import CBCWaferFromMapFile, CBCChipFromResultFile
from datetime     import date
from dateutil     import parser
from optparse     import OptionParser

import os



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <path to CBC data>, ...", version="1.1")

   p.add_option( '-v','--ver',
               type    = 'string',
               default = '1.0',
               dest    = 'version',
               metavar = 'STR',
               help    = 'Version type of the components; it is superseed by the version in the sinf file.')
   
   p.add_option( '-d','--desc',
               type    = 'string',
               default = None,
               dest    = 'description',
               metavar = 'STR',
               help    = 'Input a description for the data to be uploaded. The description of the chips is superseed with the TYPE and the LOT from the sinf file.')
   
   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'Overwirtes the account name put in the RECORD_INSERTION_USER column.')
   
   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component production.')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--update',
               action  = 'store_true',
               default = False,
               dest    = 'update',
               help    = 'Force the upload of components in update mode')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database.')
   
   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data.')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data.')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')

   upload_mode = 'update' if opt.update else 'insert'

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   
   date_of_production = date.strftime(parser.parse(opt.date),'%Y-%m-%d') \
                            if opt.date!='' else None
   

   for p in args: test_files = search_files(p,'*.xls')+search_files(p,'*.xlsx')+search_files(p,'*.csv')
   for p in args: pdf_files  = search_files(p,'*.pdf')
   for p in args: sinf_files = search_files(p,'*.sinf')


   # Wafer default configuration
   wafer_conf = {
      'kind_of_part'    : 'CBC3 Wafer',
      'unique_location' : 'CERN',
      'version'         : opt.version,
      'description'     : opt.description,
      'product_date'    : date_of_production,
      'inserter'        : opt.inserter,
      'attributes'      : [('Status','Bad')]  # Default Status for Wafer is Bad
   }

   # Chip default configuration
   cbc_chip_conf = {
      'kind_of_part'    : 'CBC3 Readout Chip',
      'unique_location' : 'CERN',
      'version'         : opt.version,
      'description'     : opt.description,
      'product_date'    : date_of_production,
      'inserter'        : opt.inserter
   }

   data = join_data_files(test_files,pdf_files,sinf_files)
   
   files_to_be_uploaded = []

   for batch in data.keys():
      chip_table = data[batch][0]
      wafer_map  = data[batch][1]
      wafer_sinf = data[batch][2]
      cbcwafer = CBCWaferFromMapFile(batch,wafer_conf,wafer_map,wafer_sinf)
      wafer_file = cbcwafer.dump_xml_data()
      if wafer_file!=None:
         #Wafer not registered
         files_to_be_uploaded.append(wafer_file)
         
      # chips will be always processed becasue registration could be done in incremental mode
      cbcchip  = CBCChipFromResultFile(batch,cbc_chip_conf,chip_table,wafer_sinf)
      files_to_be_uploaded.append(cbcchip.dump_xml_data(chip_wafer=cbcwafer))
   
   # Upload files in the database 
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=True)
   if opt.upload:  
      for fi in files_to_be_uploaded:
         # Files upload
         db_loader.upload_data(fi)