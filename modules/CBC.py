#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Mar-2019

# This module contains the classes that produce the XML file for uploading
# the CBC chip and the CBC Wafer data.


from BaseUploader import BaseUploader
from DataReader   import *
from progressbar  import *
from lxml         import etree
from shutil       import copyfile
import os

class CBCWafer(BaseUploader):
    """Class to produce the xml file for the DBloader upload. It handles the
      CBC Wafer component data read from a worksheet."""

    def __init__(self, bname, cdefault, dreader):
        """Constructor: it requires the batch production name (bname), the dict
         for configuration (cdefault) defining the ancillary data needed for the
         database upload and the sinf data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            kind_of_part:    the part to be inserted in the database;
            version:         the version of CBC chip produced;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
      """
        import copy
        cdict = copy.deepcopy(cdefault)
        
        if 'manufacturer' not in cdict.keys():
            cdict['manufacturer'] = 'Global Foundries'
                
        version = dreader.getDataFromTag('DEVICE')
        lot     = dreader.getDataFromTag('LOT')
        wafer   = dreader.getDataFromTag('WAFER')
        
        if version!=None:  cdict['version'] = version
        if lot    !=None:  
            cdict['batch_number'] = version + " Lot " + lot
            cdict['attributes'] = [('Status','Good')]

        BaseUploader.__init__(self, bname, cdict, dreader)
        
        self.batch_production = bname if wafer==None else wafer
        self.lot = lot
        self.mandatory += ['manufacturer', 'kind_of_part', 'version',
         'unique_location', 'locations']

    def serial_number(self):
        """Produces the serial number of the CBC wafer."""
        return '%s' % self.batch_production

    def name_label(self):
        """Produces the name label for the CBC wafer."""
        # Exploit same convention for the SSA and MPA for the name label
        #tag = self.manufacturer_tags[self.configuration['manufacturer']]
        #typ = self.retrieve('version').replace('.', '')
        #return '%s_%s_%s' % (typ, tag, self.serial_number())
        serial = self.serial_number()
        lot = self.lot
        return f'{serial}_{lot}'

    def dump_xml_data(self, filename=''):
        """Writes the wafer data in the XML file for the database upload. It
         requires in input the name of the XML file to be produced."""
        
        inserter  = self.retrieve('inserter')
        root = etree.Element('ROOT')
        if inserter is not None:
            root.set('operator_name',inserter)

        part_id = self.db.component_id(self.name_label(),'name_label')
        if part_id==None: 
            # not registered:
            print(self.name_label())
            self.xml_builder(root)
        
            if filename == '':
                filename = '%s_CBCwafers.xml' % self.batch_production
            with open(filename, 'w') as (f):
                f.write(etree.tostring(root, pretty_print=True, xml_declaration=True, standalone='yes').decode('UTF-8'))
            from zipfile import ZipFile
            zipfilename = os.path.splitext(filename)[0] + '.zip'
            with ZipFile(zipfilename, 'w') as (zip):
                zip.write(filename)
                print (self.mapfile)
                zip.write(os.path.basename(self.mapfile))
            os.remove(filename)
            os.remove(os.path.basename(self.mapfile))
        
            print(f'{self.name} processed!')
            return zipfilename
        
        print(f'{self.name} already registered!')
        return None


class CBCChip(BaseUploader):
    """Class to produce the xml file for the DBloader upload. It handles the
      CBC chip component data read from a worksheet."""

    def __init__(self, bname, cdefault, dreader, sinf):
        """Constructor: it requires the batch production name (bname), the dict
         for configuration (cdefault) defining the ancillary data needed for the
         database upload, the worksheet data (dreader), and the sinf file (sinf)
         with the map of the dies on the wafer.
         superseed the account name int eh RECORD_INSERTION_USER column.
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            kind_of_part:    the part to be inserted in the database;
            version:         the version of CBC chip produced;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
      """
        import copy
        cdict = copy.deepcopy(cdefault)
        
        if 'manufacturer' not in cdict.keys():
            cdict['manufacturer'] = 'Global Foundries'
        
        BaseUploader.__init__(self, bname, cdict, dreader)
        
        self.reader = dreader
        self.batch_production = bname
        
        self.mandatory += ['manufacturer', 'kind_of_part', 'version',
         'unique_location', 'locations']
        
        
        self.position_map = {}
        sinf_map = None
        try:
            keywords = ['FNLOC', 'ROWCT', 'COLCT', 'RowData', 'DEVICE', 'LOT', 'WAFER']
            data = {'RowData': ''}
            row = 0
            fp = open(sinf)
            if self.verbose:
                print ('Processing sinf file %s' % sinf)
            for line in fp:
                if self.verbose:
                    print (line.strip())
                ltag = line.strip().split(':')[0]
                ldata = line.strip().split(':')[1]
                if ltag == 'RowData':
                    if sinf_map == None:
                        sinf_map = [ [ 0 for c in range(data['COLCT']) ] for r in range(data['ROWCT']) ]
                    for col in range(data['COLCT']):
                        sinf_map[row][col] = 0 if ldata.split(' ')[col] == '__' else 1

                    row += 1
                    
                for k in keywords:
                    if k in line.split(':')[0]:
                        if k=='RowData':
                            data[k] = data[k] + line.split(':')[1]
                        elif k=='DEVICE' or k=='LOT' or k=='WAFER':
                            data[k] = line.split(':')[1].rstrip()
                        else:
                            data[k] = int(line.split(':')[1])
        finally:
            fp.close()
            if self.verbose:
                for r in range(data['ROWCT']):
                    print (sinf_map[r])

        row_range = ()
        col_range = ()
        if data['FNLOC'] == 90:
            row_range = range(data['ROWCT'] - 1, -1, -1)
            col_range = range(data['COLCT'] - 1, -1, -1)
        if data['FNLOC'] == 270:
            row_range = range(data['ROWCT'])
            col_range = range(data['COLCT'])
        pos = 1
        for row in row_range:
            for col in col_range:
                if sinf_map[row][col] != 0:
                    self.position_map[pos] = (
                     ('Row {:d}').format(row), ('Col {:d}').format(col))
                    pos += 1

        if self.verbose:
            print (self.position_map)
        
        try:    version = data['DEVICE']
        except: version = None
        try:    self.lot = data['LOT']
        except: self.lot = None
        try:    self.batch_production = data['WAFER']
        except: self.batch_production = bname
        
        if version!=None:  self.update_configuration('version', version)
        if self.lot!=None:
            self.update_configuration('batch_number', f'{version} Lot {self.lot}')
            self.update_configuration('description' , f'{version} Lot {self.lot}')
        

    def serial_number(self, n):
        """Produces the serial number of the CBC chip."""
        return ('{:d}').format(n)

    def name_label(self, n):
        """Produces the name label for the CBC chip."""
        # Exploit same convention for the SSA and MPA for the name label
        #tag = self.manufacturer_tags[self.configuration['manufacturer']]
        #typ = self.retrieve('version').replace('.', '')
        #return ('{}_{}_{}_{:08d}').format(tag, self.batch_production, typ, n)
        return ('{}_{}_{:08d}').format(self.batch_production, self.lot, n)

    def dump_xml_data(self, filename='', chip_wafer=None):
        """Writes the wafer data in the XML file for the database upload. It
         requires in input the name of the XML file to be produced."""
        
        inserter  = self.retrieve('inserter')

        root = etree.Element('ROOT')
        if inserter is not None:
            root.set('operator_name',inserter)

        self.xml_builder(root, chip_wafer)
            
        if filename == '':
            filename = '%s_CBCchip.xml' % self.batch_production
        with open(filename, 'w') as (f):
            f.write(etree.tostring(root, pretty_print=True, xml_declaration=True, standalone='yes').decode('UTF-8'))
        return filename

class CBCWaferFromMapFile(CBCWafer):
    """Produces the XML file from the Wafer map file."""

    def __init__(self, bname, cdefault, map_file, sinf_file):
        """Constructor: it requires the full (path+filename) pdf map file name."""
        reader = TableReader(sinf_file,m_rows=11,tabSize=9,csv_delimiter=":")
        CBCWafer.__init__(self, bname, cdefault, reader)
        self.mapfile = map_file
        return

    def xml_builder(self, root_tree, *args):
        """Process data, reader is not used."""
        parts = etree.SubElement(root_tree, 'PARTS')
        Serial = self.serial_number()
        NameLabel = self.name_label()
        filename = os.path.basename(self.mapfile)
        filetime = os.path.getmtime(self.mapfile)
        from datetime import datetime
        dt = datetime.fromtimestamp(filetime)
        filedate = dt.strftime('%Y-%m-%d %H:%M:%S')
        copyfile(self.mapfile, filename)
        print("file copied")

        e_data = [('PDF_MAP_FILE', filename), ('PDF_MAP_DATE', filedate)]

        self.build_parts_on_xml(parts, serial=Serial, name_label=NameLabel, extended_data=e_data)

class CBCChipFromResultFile(CBCChip):
    """Produces the XML file from the Excel spreadsheet that contains the chip
      serial numbers and the functional test result."""

    def __init__(self, bname, cdict, result, sinf):
        """Constructor: it requires the same arguments of the base class."""
        extension = os.path.splitext(result)[1]
        offset    = 0 if extension=='.csv' else 2
        reader    = TableReader(result,t_offset=offset,csv_delimiter=',')
        CBCChip.__init__(self, bname, cdict, reader, sinf)

    def xml_builder(self, root_tree, *args):
        """Process data in the reader."""
        chip_wafer = args[0]
        parts = etree.SubElement(root_tree, 'PARTS')
        data = self.reader.getDataAsCWiseDict()
        #chip_positions = getColumnData(data, 'position')  Do not use position column anymore
        chip_test_results = getColumnData(data, 'result')
        if chip_test_results==None: chip_test_results = getColumnData(data, 'status')
        
        chip_id = getColumnData(data, '^ID')
        if chip_id==None: chip_id = getColumnData(data, 'chip id')  # new csv format has "chip id"
        
        chip_serials = []
        if checkForHexValue(data, '^ID'):
            chip_serials = [ int(s, 16) for s in chip_id ]
        else:
            chip_serials = [ int(s) for s in chip_id ]
            
        Nup = len(chip_serials)
        Wig = [f'Writing CBC chips of {self.batch_production}: ', Bar('=', '[', ']'), ' ', Percentage()]

        for i in progressbar(range(Nup), widgets=Wig, redirect_stdout=True):
            chip = chip_serials[i]            
            Serial = self.serial_number(chip)
            NameLabel = self.name_label(chip)
            
            part_id = self.db.component_id(NameLabel,'name_label')
            if part_id!=None:
                # registered
                print(f'{NameLabel} processed!')
                continue
            
            result = chip_test_results[i]
            result_tag = 'Good' if result == 'good' or result =='pass' else 'Bad'
            #position = int(chip_positions[i])
            position = i+1
            position_tag = self.position_map[position]
            chip_attributes = [('Status', result_tag)]
            if position_tag != None:
                chip_attributes.append(('Chip Row on Wafer', position_tag[0]))
                chip_attributes.append(('Chip Column on Wafer', position_tag[1]))
            self.configuration['attributes'] = chip_attributes
            if chip_wafer is not None:
                w_kop = chip_wafer.retrieve('kind_of_part')
                w_serial = chip_wafer.serial_number()
                wp = etree.SubElement(parts, 'PART', mode='auto')
                etree.SubElement(wp, 'KIND_OF_PART').text = w_kop
                etree.SubElement(wp, 'SERIAL_NUMBER').text = w_serial
                children = etree.SubElement(wp, 'CHILDREN')
                self.build_parts_on_xml(children, serial=Serial, name_label=NameLabel)
            else:
                self.build_parts_on_xml(parts, serial=Serial, name_label=NameLabel)

            print(f'{NameLabel} processed!')

        return
