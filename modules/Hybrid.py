#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 13-Nov-2019

# This module contains the classes that produce the XML file for uploading
# the Hybrid components.

from unittest.mock import NonCallableMagicMock
from BaseUploader import BaseUploader, ConditionData
from lxml         import etree
from shutil       import copyfile
from copy         import deepcopy
from Exceptions   import *
from Utils        import search_files
from enum         import Enum
from datetime     import datetime

import json


HybridTest = Enum('HybridTest','Electrical, Visual')
ReferenceT = Enum('ReferenceT','POH_acc, FEH_acc, SEH_acc, ROH_acc')
EChipTypes = Enum('EChipTypes','CBC, SSA, CIC')

class FEHybrid(BaseUploader):
   """Class to produce the xml file for the DBloader upload. It handles the
      FE Hybrid component data."""

   def __init__(self, name , cdict):
      """Constructor: it requires the instance name, the dict for configuration
         (cdict) defining the ancillary data needed for the database upload. 
         Mandatory parameters in configuration:
            manufacturer:      the manufacturer of the component;
            kind_of_part:      the part to be inserted in the database;
            version:           the version of 8CBC3 hybrid produced;
            unique_location:   the location where all the components are;
      """
      BaseUploader.__init__(self, name, cdict)

      self.mandatory += ['manufacturer','kind_of_part','version',\
                         'unique_location']

   def dump_xml_data(self, filename='', uploaded=[], excluded=[]):
      """Writes the wafer data in the XML file for the database upload.
         Optional input parameters:
           filename    name of the xml file to be produced;
           excluded    list of serial IDs to be excluded from the upload; 
         Output parameter: 
           uploaded    list of serial id written in the file."""
      
      
      inserter  = self.retrieve('inserter')
      root_node = 'ROOT' if inserter is None else f'ROOT operator_name="{inserter}"'
      root      = etree.Element(root_node)
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree,uploaded,excluded)

      if filename == '':
         filename = '%s_FEHybrids.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                        xml_declaration = True,\
                                        standalone="yes").decode('UTF-8') )

      return filename



class Upload8CBC3(FEHybrid):
   """Produces the XML file to register or to update the 8CBC3 FE Hybrid components.""" 

   allowed_values = { 
                      'status' : ['Good','Bad'],
                      'chip_type' : ['CBC3.0','CBC3.1'],
                      'sensor_spacing' : ['1.8 mm'],
                      'stiffener_type' : ['FR4','CF-K13D2U'],
                      'assembly_state' : ['Fully assembled','Bare','Dummy hybrid'],
                      'manufacturer'   : ['Valtronic','AEMtec']
                    }

   def __init__(self, serial, cdict, status=None, chipType=None, sensorSpacing=None,\
                stiffenerType=None, assemblyState=None, comment=None):
      """Constructor: it requires the serial number of the FE Hybrid to upload and the
         configuration dictionary. Optional parameneters are:
           status             [Good,Bad]
           chip_type          the type of the chip [CBC3.0,CBC3.1];
           sensor_spacing     the sensor spacing [1.8 mm];
           stiffener_type     the stiffener type [FR4,CF-K13D2U]
           assembly_state     [Fully assembled,Bare,Dummy hybrid];
           comment            description of the component.
      """
      FEHybrid.__init__(self, serial, cdict)

      # Override the configuration dictionary with the input parameters
      if status!=None:        self.update_configuration('status',status)
      if chipType!=None:      self.update_configuration('chip_type',chipType)
      if sensorSpacing!=None: self.update_configuration('sensor_spacing',sensorSpacing)
      if stiffenerType!=None: self.update_configuration('stiffener_type',stiffenerType)
      if assemblyState!=None: self.update_configuration('assembly_state',assemblyState)
      if comment!=None:       self.update_configuration('description',comment)

      # Check attribute  and configuration values
      c_status = self.retrieve('status')
      if c_status!=None and c_status not in self.allowed_values['status']:
         raise NotRecognized('FEHybrid',c_status,'as Status attribute')

      c_chip = self.retrieve('chip_type')
      if c_chip!=None and c_chip not in self.allowed_values['chip_type']:
         raise NotRecognized('FEHybrid',c_chip,'as Chip type attribute')

      c_spacing = self.retrieve('sensor_spacing')
      if c_spacing!=None and c_spacing not in self.allowed_values['sensor_spacing']:
         raise NotRecognized('FEHybrid',c_spacing,'as Sensor spacing attribute')

      c_stiffener = self.retrieve('stiffener_type')
      if c_stiffener!=None and c_stiffener not in self.allowed_values['stiffener_type']:
         raise NotRecognized('FEHybrid',c_stiffener,'as Stiffener type attribute')

      c_assembly = self.retrieve('assembly_state')
      if c_assembly!=None and c_assembly not in self.allowed_values['assembly_state']:
         raise NotRecognized('FEHybrid',c_assembly,'as Assembly state attribute')

      c_manuf = self.retrieve('manufacturer')
      if c_manuf!=None and c_manuf not in self.allowed_values['manufacturer']:
         raise NotRecognized('FEHybrid',c_manuf,'as manufacturer')


      # Override version in configuration dictionary
      if c_chip!=None and c_spacing!=None and c_stiffener!=None:
         version_str = '{}, {}, {}'.format(c_chip,c_spacing,c_stiffener)
         self.update_configuration('version',version_str)



   def xml_builder(self,parts,*args):
      """Process data."""
      # Gather the list of components to be excluded
      excluded = args[1]

      # Gather the serial number of component
      Serial = self.name
      if Serial in excluded: return

      c_status    = self.retrieve('status')
      c_chip      = self.retrieve('chip_type') 
      c_spacing   = self.retrieve('sensor_spacing')
      c_stiffener = self.retrieve('stiffener_type')
      c_assembly  = self.retrieve('assembly_state')

      hybrid_attributes = []
      if c_status!=None:    hybrid_attributes.append( ('Status',c_status) )
      if c_chip!=None:      hybrid_attributes.append( ('Chip type',c_chip) )
      if c_spacing!=None:   hybrid_attributes.append( ('2S Sensor spacing',c_spacing) )
      if c_stiffener!=None: hybrid_attributes.append( ('Stiffener type',c_stiffener) )
      if c_assembly!=None:  hybrid_attributes.append( ('Assembly state',c_assembly) )
      if len(hybrid_attributes)!=0: 
         self.configuration['attributes']  = hybrid_attributes

      self.build_parts_on_xml(parts,serial=Serial)

      try:
         args[0].append( Serial )
      except:
         pass



class Hybrid():
   """Class to upload the Hybrid data into the database."""
   
   associated_electronic = {
         '2S Front-end Hybrid' : { EChipTypes['CBC'].name : 'CBC3 Readout Chip' ,
                                   EChipTypes['CIC'].name : 'CIC Chip' },
         'PS Front-end Hybrid' : { EChipTypes['SSA'].name : 'SSA Chip' , 
                                   EChipTypes['CIC'].name : 'CIC Chip' },
         '2S Service Hybrid'   : {}, #LPGBT
         'PS Read-out Hybrid'  : {}, #LPGBT
         'PS Power Hybrid'     : {}
                           }
   
   def __init__(self, serial):
      """Constructor: it requires the serial number that is used to decode the hybrid properties."""
      try:
         self.serial = serial                  # stores the serial number
         self.h_type = serial.split('-')[0]    # stores the first section of the serial number
         self.h_kind = None                    # stores the kind_of_part
         self.h_tick = None                    # stores the spacing code
         self.h_side = None                    # stores the side code
         self.h_code = serial.split('-')[1]    # stores the last section of the serial number
         self.h_vend = None                    # stores the vendor name
         self.h_lot  = self.h_code[1:3]        # stores the batch number
      
         if self.h_code[0]=='2': self.h_vend = 'Valtronic'
         if self.h_code[0]=='3': self.h_vend = 'AEMtec'

         if '2SFEH' in self.h_type or '2S-FEH' in self.h_type: 
            self.h_kind = '2S Front-end Hybrid' 
            self.h_tick = self.h_type[-3:-1]
            self.h_side = self.h_type[-1] 

         if '2SSEH' in self.h_type or '2S-SEH' in self.h_type: 
            self.h_kind = '2S Service Hybrid' 
                                                                            
         if 'PSFEH' in self.h_type or 'PS-FEH' in self.h_type:  
            self.h_kind = 'PS Front-end Hybrid' 
            self.h_tick = self.h_type[-3:-1] 
            self.h_side = self.h_type[-1]

         if 'PSROH' in self.h_type or 'PS-ROH' in self.h_type: 
            self.h_kind = 'PS Read-out Hybrid' 
            self.h_tick = self.h_type[-3:-1] 

         if 'PSPOH' in self.h_type or 'PS-POH' in self.h_type: 
            self.h_kind = 'PS Power Hybrid' 

      except:
         raise NotRecognized(self.__class__.__name__,serial,\
                     'as serial number for any Hybrid type!')
   
   def has_electronic(self,chip_type):
      """Returns True is this hybrid type contains the given chip_type.
         On the contrary it returns False."""
      ct = None
      try:
         ct = EChipTypes[str(chip_type)]  # check if chip type is recognized
      except:
         types = [el.name for el in EChipTypes]
         print(Fore.RED+f'{chip_type}'+Style.RESET_ALL+' is not a valid ID type!')
         print('Valid chip types are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
         exit(1)
         
      if ct.name in self.associated_electronic[self.h_kind].keys():  return True
      
      return False
      





class HybridComponentT(Hybrid,BaseUploader):
   """Produces the XML file to register any Hybrid components.""" 
                        #    VISUAL INSPECTION     ELECTRICAL TEST    STATUS
   status_logic      = { ('Good'                   , 'Good'        ) : 'Good',
                         ('Bad'                    , 'Good'        ) : 'Bad',
                         ('Needs repair'           , 'Good'        ) : 'Needs repair',
                         ('Needs expert inspection', 'Good'        ) : 'Needs retest',
                         ('Good'                   , 'Bad'         ) : 'Bad',
                         ('Bad'                    , 'Bad'         ) : 'Bad',
                         ('Needs repair'           , 'Bad'         ) : 'Needs repair',
                         ('Needs expert inspection', 'Bad'         ) : 'Bad',
                         ('Good'                   , 'Needs retest') : 'Needs retest',
                         ('Bad'                    , 'Needs retest') : 'Bad',
                         ('Needs repair'           , 'Needs retest') : 'Needs repair',
                         ('Needs expert inspection', 'Needs retest') : 'Needs retest',
                         ('Good'                   , ''            ) : 'Good',
                         ('Bad'                    , ''            ) : 'Bad',
                         ('Needs repair'           , ''            ) : 'Needs repair',
                         ('Needs expert inspection', ''            ) : 'Needs retest',
                         (''                       , 'Good'        ) : 'Good',
                         (''                       , 'Bad'         ) : 'Bad',
                         (''                       , 'Needs retest') : 'Needs retest'
                       }
   
   
   def __init__(self, serial, location, comment=None, inserter=None):
      """Constructor for component registration: it requires the serial number of the Hybrid and
         the hybrid location to produce the configuration dictionary. 
         Optional parameneters are:
           comment:      description of the component.
           inserter:     person recording the data (override the CERN user name)
      """
      Hybrid.__init__(self,serial)

      name = '{}'.format(serial)
      cdict = {}
      self.attributes = []
      self.allowed_attributes = {}

      # Hybrid configuration
      cdict['kind_of_part']    = self.h_kind
      cdict['unique_location'] = location 
      cdict['manufacturer']    = self.h_vend
      cdict['batch_number']    = self.h_code[1:3]
      # cdict['version']         = 'Empty'    Should we define a version string?

      if comment!=None: cdict['description'] = comment
      if inserter!=None: cdict['inserter'] = inserter

      #print (cdict)
      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['manufacturer','kind_of_part','unique_location']

      # Retrieve the allowed attributes for this specific type
      qy = f'select * from {self.db.database}.trkr_attributes_v a where a.kind_of_part=\'{self.h_kind}\''
      attributes = self.db.data_query(qy)
      for a in attributes:
         self.allowed_attributes[ a['attributeName'] ] = a['allowedValues'].split(',')
      
      # Qualify the Hybrid   
      if self.h_kind == '2S Front-end Hybrid':         # Attributes not defined for 2S-FEH:
                                                       #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   CIC Version [CIC1 , CIC2]
         if self.h_tick=='18':  self.load_attribute('2S Sensor spacing','1.8 mm')
         if self.h_tick=='40':  self.load_attribute('2S Sensor spacing','4.0 mm')
         if self.h_side=='R':   self.load_attribute('FE Hybrid Side','Right')
         if self.h_side=='L':   self.load_attribute('FE Hybrid Side','Left')

      if self.h_kind == '2S Service Hybrid' :          # Attributes not defined for 2S-SEH:
         pass                                          #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   LPGBT Version [V0 , V1]
                                                      
      if self.h_kind == 'PS Front-end Hybrid':         # Attributes not defined for PS-FEH:
                                                       #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   SSA Version [SSA1 , SSA2]
                                                       #   CIC Version [CIC1 , CIC2]
         if self.h_tick=='16':  self.load_attribute('PS Sensor spacing','1.6 mm')
         if self.h_tick=='26':  self.load_attribute('PS Sensor spacing','2.6 mm')
         if self.h_tick=='40':  self.load_attribute('PS Sensor spacing','4.0 mm')
         if self.h_side=='R':   self.load_attribute('FE Hybrid Side','Right')
         if self.h_side=='L':   self.load_attribute('FE Hybrid Side','Left')

      if self.h_kind == 'PS Read-out Hybrid':          # Attributes not defined for PS-ROH:
                                                       #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   LPGBT Version [V0 , V1]
                                                       #   LpGBT Bandwidth [5Gbps , 10Gbps]
         if self.h_tick=='16':  self.load_attribute('PS Sensor spacing','1.6 mm')
         if self.h_tick=='26':  self.load_attribute('PS Sensor spacing','2.6 mm')
         if self.h_tick=='40':  self.load_attribute('PS Sensor spacing','4.0 mm')

      if self.h_kind == 'PS Power Hybrid':             # Attributes not defined for PS-POH:
         pass                                          #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]


   def registered_test_status(self):
      """Returns an tuple with the Visual inspection status and the Electrical
         test status."""
      e_status = ''
      v_status = ''
      h_data   = None
      db = self.db.database
      qy = f'select * from {db}.parts p where p.serial_number=\'{self.serial}\''
      rs = self.db.data_query(qy)
      try:
         data = rs[0]
         kopid = str(data['kindOfPartId'])
         qy = f'select * from {db}.p{kopid} p where p.serial_number=\'{self.serial}\''
         try:
            h_data = self.db.data_query(qy)[0]
         except Exception as e:
            print(e)
            exit(1)
      except:
         print(f"component {self.serial} not registered in {db}")
         return (v_status,e_status)
      
      try:
         e_status = h_data['aelectricalTestStatus']
      except KeyError:
         pass
      try:
         v_status = h_data['avisualInspectionStatus']
      except KeyError:
         pass
      
      return (v_status, e_status)
   
   
   def component_status_from_tests(self,electrical_status='',visual_status=''):
      """Returns the component status from the electrical and visual test 
         results. The database is checked is any of the result is not given."""
      
      registered = self.registered_test_status()
      key_status = (visual_status     if visual_status!=''     else registered[0],
                    electrical_status if electrical_status!='' else registered[1])
      try:
         return self.status_logic[key_status]
      except KeyError:
         txt = f'(visual status=\'{key_status[0]}\' , electrical status=\'{key_status[1]}\')'
         raise NotRecognized('HybridComponentT',txt,'as valid combination.')

   def load_attribute(self, name, value):
      """Load a component attribute. Check the allowed attributes."""
      allowed_names = self.allowed_attributes.keys()
      if name not in allowed_names:
         raise BadParameters(self.__class__.__name__,f'{name} is not an attribute of {self.h_kind}.')
      allowed_values = self.allowed_attributes[name]
      if value not in allowed_values:
         raise BadParameters(self.__class__.__name__,f'{value} is not allowed for {name} attribute.')
      self.attributes.append( (name, value) )
      self.update_configuration('attributes',self.attributes)


   def xml_builder(self,parts):
      """Process data."""
      # Gather the serial number of component
      Serial = self.name.split('_')[0]
      self.build_parts_on_xml(parts,serial=Serial)


   def dump_xml_data(self, filename='', tag=''):
      """Writes the hybrid data in the XML file for the database upload."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree)

      if filename == '':
         filename = f'{self.name}{tag}.xml'
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                        xml_declaration = True,\
                                        standalone="yes").decode('UTF-8') )
      return filename


class HybridSubComponentT(Hybrid,BaseUploader):
   """Produces the XML file to associate any Hybrid components.""" 
   
   def __init__(self, serial, location, inserter=None):
      """Constructor for sub component association: it requires the serial 
         number of the Hybrid.
         Optional parameneters are:
           inserter:     person recording the data (override the CERN user name)"""
   
      Hybrid.__init__(self,serial)

      name = f'{serial}'
      # Electronics is internally described with a tuple of 4 values:
      # (kind of part , part id , serial number , position on circuit , status)
      self.electronics = []
      
      # Hybrid configuration
      BaseUploader.__init__(self, f'{serial}', {'inserter':inserter})
      self.mandatory += ['kind_of_part','unique_location']
      
      # Set the location gined to the class
      self.update_configuration('unique_location', location)
      
      db = self.db.database
      qy = f'select * from {db}.parts p where p.serial_number=\'{self.name}\''
      rs = self.db.data_query(qy)
      
      try:
         data = rs[0]         
         part_location = self.locations[data['locationId']]['locationName']
         if part_location!=location:
            msg = Fore.RED+'Component '+Fore.BLUE+f'{self.name}'+ Fore.RED+\
            f' is registered with a location ({part_location}) different from the one given by the gui ({location}).'
            raise BadParameters(self.__class__.__name__,msg)
      except IndexError:
         print( 'Component '+Fore.BLUE+f'{self.name}'+ Style.RESET_ALL+f' is not yet registered in database {db}!' )
      
      
   def load_electronics(self,electronics=[], electronic_type=''):
      """Process the associated electronics if it belongs to this Hybrid type.
         It requires the list of electronic components (electronics) each one 
         defined by the pair of its serial number and its position on the flex
         circuit, plus the electonic type.""" 
      
      # Do nothing if the electronics chips does not belong to this type 
      if not self.has_electronic(electronic_type):   return
      if len(electronics)==0:   return

      # Check input type
      if not all(isinstance(item,tuple) and len(item)==2 for item in electronics):
         raise BadParameters(self.__class__.__name__,f'electronics is not a list of pairs.')

      db = self.db.database
      
      # Get the electronics kopid
      ekop = self.associated_electronic[self.h_kind][electronic_type]
      eser = ''
      for chip in electronics:
         serial = chip[0]   
         eser+=f' p.serial_number=\'{serial}\' or'
      qy = f'select * from {db}.parts p where p.kind_of_part=\'{ekop}\' and ({eser[:-3]})'

      data = []
      kop_id = None
      rs = self.db.data_query(qy)
      try:
         data = rs[0]  
         kop_id = str(data['kindOfPartId'])
      except Exception as e:
         print(e)   
         pass

      # Get electronics status
      for chip in electronics:
         status  = 'UNDEFINED'
         serial  = chip[0]
         part_id = str(next((el['id'] for el in rs if el['serialNumber']==chip[0]), ''))
         try:
            qy = f'select * from {db}.p{kop_id} p where p.id=\'{part_id}\''
            data = self.db.data_query(qy)[0]
            status = data['astatus']
         except Exception as e:
            pass
         self.electronics.append( (ekop , str(part_id), chip[0] , chip[1] , f'{status}') )
      

   def component_status_from_electronics(self):
      """Returns the global status of the associated electronics."""
      if len(self.electronics)==0:   return 'Good'
      return 'Good' if all( (chip[4]=='Good') for chip in self.electronics ) else 'Bad'


   def dump_xml_data(self, filename='',tag=''):
      """Writes the electronics data in the XML file for the database upload.
         Optional input parameters:
           tag          tag appaned to the filename; """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree)

      if filename == '':   filename = f'{self.name}{tag}.xml'
      else:                filename = f'{filename}{tag}.xml'
      
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      return filename
   
   
   def xml_builder(self,parts):
      """Process data."""
      # Gather the serial number of component
      hybrid = etree.SubElement(parts, "PART", mode="auto")
      etree.SubElement(hybrid,"KIND_OF_PART").text  = self.h_kind
      etree.SubElement(hybrid,"SERIAL_NUMBER").text = self.name
      children = etree.SubElement(hybrid,"CHILDREN")

      for chip in self.electronics:
         attributes = []
         if chip[3]!='':   attributes.append(('Chip Posn on Flex Hybrid',chip[3]))
         #if chip[4]!='UNDEFINED':   attributes.append(('Status',chip[4]))
         
         if chip[1]!='':
            self.update_configuration('kind_of_part', chip[0])
            self.update_configuration('attributes' ,attributes)
            self.update_parts_on_xml(children,chip[1],serial=chip[2])




class AttachChildrenToParent(BaseUploader):
   """Class to produce the xml file to update the parent <-> child relationship
      of components. The serial number of parents are given in input, while the
      serial number of children are taken from external sources.
   """

   def __init__(self, name , cdict):
      """Constructor: it requires the instance name, the dict for configuration
         (cdict) defining the ancillary data needed for the database upload.
         Mandatory parameters in configuration are:
            kind_of_part:      the kind of part of children;
            unique_location:   the location where the children components are;
      """
      BaseUploader.__init__(self, name, cdict)

      self.mandatory += ['kind_of_part','unique_location']

   def dump_xml_data(self, filename='', associated=[], excluded=[]):
      """Writes the wafer data in the XML file for the database upload.
         Optional input parameters:
           filename     name of the xml file to be produced;
           excluded     list of parent serial IDs to be excluded from the upload; 
         Output parameter: 
           associated   list of parent serial id associated with chips."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree,associated,excluded)

      if filename == '':
         filename = '%s_chip_association.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      return filename



class AttachCBC2FEH(AttachChildrenToParent):
   """Class to produce the xml file to update the parent <-> child relationship
      among FE Hybridis and its chips. The serial numbers of the chips attached 
      to the hybrid component are read from the output of the calibration / test
      runs.
   """

   def __init__(self, serial, kop, logs_dir, number_of_cbc, cdict):
      """Constructor: it requires the serial number of the FE Hybrid (serial), the 
         kind of part of the FE Hybrid (kop), the directory where the output from 
         calibration / test runs are located (logs_dir), the number of CBC in the 
         circuit (number_of_cbc) and the dict for configuration (cdict) defining 
         the ancillary data needed.
      """
      AttachChildrenToParent.__init__(self,serial,cdict)

      self.log_files  = search_files(logs_dir,pattern='FE0CBC[0-7].txt')
      self.parent_kop = kop #'8CBC3 Front-end Hybrid'
      self.number_of_cbc = number_of_cbc

      self.update_configuration('kind_of_part','CBC3 Readout Chip')


   def xml_builder(self,parts,*args):
      """Process data."""
      # Gather the list of parents to be excluded
      excluded = args[1]

      # Gather the serial number of parent
      Serial = self.name
      if Serial in excluded: return

      # Filter out the list of log file using the serial number of the parent
      cbc_log_to_process = [f for f in self.log_files if Serial in f]
      cbc_log_to_process.sort()
      latest_cbc_files = cbc_log_to_process[-self.number_of_cbc:]

      # If less than 8 files exit and not produce XML
      if len(latest_cbc_files)!=self.number_of_cbc:
        msg = 'Number of expected CBC calibration file is {},  but {} were found!'.\
              format(self.number_of_cbc,latest_cbc_files)
        raise BadCBCCalibration('AttachCBC2FEH',msg)

      CBCIDs = []  #contains the integer value of the CBCid of all chips

      for num, cbc_file in enumerate(latest_cbc_files):
         with open(cbc_file, 'r') as f:
            CBCid = "000000"
            for line in f:
               if 'ChipIDFuse' in line:
                  code = line.split()[4][2:]
                  if 'ChipIDFuse1' in line:  CBCid   = CBCid[:3] + code
                  elif 'ChipIDFuse2' in line:  CBCid = CBCid[:1] + code + CBCid[-2:]
                  elif 'ChipIDFuse3' in line:  CBCid = code + CBCid[-4:]

         CBCserial = int(CBCid,16)
         if CBCserial == 0:
            msg = "Found a zero CBC serial number for {} at 0x0{}".format(Serial,num+1)
            raise BadCBCCalibration('AttachCBC2FEH',msg)
         else:
            CBCIDs.append( CBCserial )

      if len(CBCIDs)!=self.number_of_cbc or len(CBCIDs) > len( set(CBCIDs) ):
         raise BadCBCCalibration('AttachCBC2FEH',\
                 'corrupted configuration for {}'.format(Serial))

      hybrid = etree.SubElement(parts, "PART", mode="auto")
      etree.SubElement(hybrid,"KIND_OF_PART").text  = self.parent_kop
      etree.SubElement(hybrid,"SERIAL_NUMBER").text = Serial
      children = etree.SubElement(hybrid,"CHILDREN")

      for num, c in enumerate(CBCIDs):
         posn = '0X0{}'.format(num+1)
         self.configuration['attributes'] = [('Chip Posn on Flex Hybrid',posn)]
         self.update_parts_on_xml(children,serial=str(c))

      try:
         args[0].append( Serial )
      except:
         pass


class RunHeader4HR(ConditionData):
   """Produces the XML file for storing the run meatadata."""

   def __init__(self, serial, run_conf):
      """Constructor: it requires the description of the run data (run_conf).

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
         run_name and run_type + run_number are exclusive. 

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            inserter:     person recording the run metadata (override the user)
      """

      name = '{}_run_metadata'.format(serial)
      ConditionData.__init__(self, name, run_conf, None)
      self.only_run = True


   def dump_xml_data(self):
      """Writes the run_header in a XML file to be uploaded in the database."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      self.xml_builder(root)

      filename = '{}.xml'.format(self.name)
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


class ReferenceCutsT(ConditionData):
   """Produces the XML file for storing the reference cuts for accepting the Hybrids."""
   data_description  = {'POH_acc':  {  
                  'kind_of_part' : 'PS Power Hybrid',    
                     'cond_name' : 'POH Test Reference',
                    'table_name' : 'REF_TEST_POH'
                                    }
                       }
   
   def __init__(self, name, run_conf, acc_type, version='v1', inserter=None):
      """Constructor: it requires the name of the csv data file, the description of the 
         run data (run_conf), the dataset version (version) and the data inserter if the user 
         account must be overwritten. The run number will be automaticallyn increased if the
         run_number parameter will not be given. 

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
         run_name and run_type + run_number are exclusive. 

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            inserter:     person recording the run metadata (override the user)
      """

      configuration = {}
      configuration.update(self.data_description[acc_type])
      configuration.update(run_conf)
      configuration['data_version']=version
      if inserter!="":  configuration['inserter']=inserter
      
      ConditionData.__init__(self, name, configuration, None)
      self.attach_part = False


   def build_data_block(self,dataset):
      """Builds the data block."""
      etree.SubElement(dataset,"DATA_FILE_NAME").text = f'{self.name}.csv'


   def dump_xml_data(self):
      """Writes the run_header in a XML file to be uploaded in the database."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      self.xml_builder(root)

      filename = '{}.xml'.format(self.name)
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )



class HybridResultsT(Hybrid,ConditionData):
   """Produces the XML file for storing the Hybrid result data."""
   
   data_description  = {('PS Front-end Hybrid','Electrical'):  {  
                                 'kind_of_part' : 'PS Front-end Hybrid',     
                                    'cond_name' : 'Tracker PS FEH Test Result',
                                   'table_name' : 'TEST_PSFEH',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : '',
                                                   'FAILURE_CAUSE'     : '',
                                                   'KNOWN_PROBLEMS'    : '',
                                                   'COMPUTER'          : '',
                                                   'LOCAL_DATA_PATH'   : '',
                                                   'START_TIME'        : '',
                                                   'END_TIME'          : '',
                                                   'TEST_CARD'         : '',
                                                   'BACKPLANE_POS'     : '',
                                                   'FC7_MACADDR'       : '', 
                                                   'SOFTWARE_VER'      : '',
                                                   'E_MEASUREMENT'     : '',
                                                   'OPENS'             : '',
                                                   'OPEN_CH'           : '',
                                                   'SHORTS'            : '',
                                                   'SHORTED_CH'        : '',
                                                   'PEDESTAL_AVG'      : '',
                                                   'PEDESTAL_RMS'      : '',
                                                   'NOISE_AVG'         : '',
                                                   'NOISE_RMS'         : '',
                                                   'CIC_INTEST'        : '',
                                                   'CIC_INBADLINES'    : '',
                                                   'CIC_OUTTEST'       : '',
                                                   'CIC_OUTBADLINES'   : '',
                                                   'SSA_OUTTEST'       : '',
                                                   'SSA_BAD_STUBLINES' : '',
                                                   'SSA_BAD_L1LINES'   : '',
                                                   'SSA_BAD_LATERAL'   : '',
                                                   'ROOT_BLOB'         : '',
	                                                'ROOT_FILE'         : '',
	                                                'ROOT_DATE'         : ''},
                                    'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   },
                        ('PS Front-end Hybrid','HighVoltage'):  {  
                                 'kind_of_part' : 'PS Front-end Hybrid',     
                                    'cond_name' : 'Tracker PS FEH HV Test Result',
                                   'table_name' : 'TEST_PSFEHRHV',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : '',
                                                   'FAILURE_CAUSE'     : '',
                                                   'KNOWN_PROBLEMS'    : '',
                                                   'COMPUTER'          : '',
                                                   'LOCAL_DATA_PATH'   : '',
                                                   'START_TIME'        : '',
                                                   'END_TIME'          : '',
                                                   'TEST_CARD'         : '',
                                                   'BACKPLANE_POS'     : '',
                                                   'FC7_MACADDR'       : '', 
                                                   'SOFTWARE_VER'      : '',
                                                   'TEMP_AVG_DEGC'     : '',
                                                   'RESISTANCE'        : '',
                                                   'RESISTANCE_OK'     : '',
                                                   'HV_CONNECTIVITY_OK': '',
                                                   'HV_LEAKCURRENT_OK' : '',
                                                   'HV_LEAKCURRENT'    : '',
                                                   'ROOT_BLOB'         : '',
	                                                'ROOT_FILE'         : '',
	                                                'ROOT_DATE'         : ''},
                                    'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   },
                        ('2S Front-end Hybrid','Electrical'):  {  
                                 'kind_of_part' : '2S Front-end Hybrid',     
                                    'cond_name' : 'Tracker 2S FEH Test Result',
                                   'table_name' : 'TEST_2SFEH',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : '',
                                                   'FAILURE_CAUSE'     : '',
                                                   'KNOWN_PROBLEMS'    : '',
                                                   'COMPUTER'          : '',
                                                   'LOCAL_DATA_PATH'   : '',
                                                   'START_TIME'        : '',
                                                   'END_TIME'          : '',
                                                   'TEST_CARD'         : '',
                                                   'BACKPLANE_POS'     : '',
                                                   'FC7_MACADDR'       : '',
                                                   'SOFTWARE_VER'      : '',
                                                   'E_MEASUREMENT'     : '',
                                                   'OPENS'             : '',
                                                   'OPEN_CH'           : '',
                                                   'SHORTS'            : '',
                                                   'SHORTED_CH'        : '',
                                                   'PEDESTAL_AVG'      : '',
                                                   'PEDESTAL_RMS'      : '',
                                                   'NOISE_AVG'         : '',
                                                   'NOISE_RMS'         : '',
                                                   'CIC_OUTTEST'       : '',
                                                   'CIC_OUTBADLINES'   : '',
                                                   'CBC_OUTTEST'       : '',
                                                   'CBC_BAD_OUTLINES'  : '',
                                                   'CBC_BAD_LATERAL'   : '',
                                                   'ROOT_BLOB'         : '',
	                                                'ROOT_FILE'         : '',
	                                                'ROOT_DATE'         : ''},
                                    'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   },
                        ('PS Read-out Hybrid'   ,'Electrical'):  {  
                                 'kind_of_part' : 'PS Read-out Hybrid',     
                                    'cond_name' : 'Tracker PS ROH Test Result',
                                   'table_name' : 'TEST_PSROH',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : '',
                                                   'FAILURE_CAUSE'     : '',
                                                   'KNOWN_PROBLEMS'    : '',
                                                   'COMPUTER'          : '',
                                                   'LOCAL_DATA_PATH'   : '',
                                                   'START_TIME'        : '',
                                                   'END_TIME'          : '',
                                                   'TEST_CARD'         : '',
                                                   'BACKPLANE_POS'     : '',
                                                   'FC7_MACADDR'       : '', 
                                                   'SOFTWARE_VER'      : '',
                                                   'E_MEASUREMENT'     : '',
                                                   'E_MEASUREMENTQLTY' : "",
                                                   'CIC_OUTTEST'       : '',
                                                   'CIC_OUTBADLINES'   : '',
                                                   'FCMD_TEST'         : "",
                                                   'FCMD_BADLINES'     : "",
                                                   'CLOCK_TEST'        : "",
                                                   'CLOCK_BADLINES'    : "",
                                                   'RESET_TEST'        : "",
                                                   'RESET_BADLINES'    : "",
                                                   'RESET_VOLTAGELVLS' : "",
                                                   'I2C_TEST'          : "",
                                                   'I2C_BADMASTERS'    : "",
                                                   'VTRX_VERSION'      : "",
                                                   'VTRX_TEST'         : "",
                                                   'LPGBT_ADCTEST'     : "",
                                                   'LPGBT_BADADCLINES' : "",
                                                   'LPGBT_ADCMVALUES'  : "",
                                                   'LPGBT_GPIOTEST'    : "",
                                                   'LPGBT_GPIOBADLINES': "",
                                                   'EYE_OPENING'       : "",
                                                   'ROOT_BLOB'         : '',
	                                                'ROOT_FILE'         : '',
	                                                'ROOT_DATE'         : ''},
                                    'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   },
                           ('PS Power Hybrid'   ,'Electrical'):  {  
                                 'kind_of_part' : 'PS Power Hybrid',     
                                    'cond_name' : 'Tracker PS POH Test Result',
                                   'table_name' : 'TEST_PSPOH',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : "",
                                                   'FAILURE_CAUSE'     : "",
                                                   'KNOWN_PROBLEMS'    : "",
                                                   'COMPUTER'          : "",
                                                   'LOCAL_DATA_PATH'   : "",
                                                   'START_TIME'        : "",
                                                   'END_TIME'          : "",
                                                   'TEST_CARD'         : "",
                                                   'BACKPLANE_POS'     : "",
                                                   'FC7_MACADDR'       : "",
                                                   'SOFTWARE_VER'      : "",
                                                   'TEST_LOAD_SETTINGS': "",
                                                   'TEST_VLTG_SETTINGS': "",
                                                   'INPUT_VOLTAGE'     : "",
                                                   'EFFICIENCY_TEST'   : "",
                                                   'VOLTAGES_0LOAD'    : "",
                                                   'CURRENTS_0LOAD'     : "",
                                                   'EFFICIENCY_0LOAD'  : "",
                                                   'VOLTAGES_50LOAD'   : "",
                                                   'CURRENTS_50LOAD'    : "",
                                                   'EFFICIENCY_50LOAD' : "",
                                                   'VOLTAGES_100LOAD'  : "",
                                                   'CURRENTS_100LOAD'  : "",
                                                   'EFFICIENCY_100LOAD': "",
                                                   'VOLTAGES_120LOAD'  : "",
                                                   'CURRENTS_120LOAD'   : "",
                                                   'EFFICIENCY_120LOAD': "",
                                                   'TEST_CARD_ERRORS'  : "",
                                                   'TURNON_VOLTAGE'    : "",
                                                   'TURNOFF_VOLTAGE'   : "",
                                                   'RESISTANCE_2V55'   : "",
                                                   'RESISTANCE_1V25R'  : "",
                                                   'RESISTANCE_1V25L'  : "",
                                                   'RESISTANCE_1V25T'  : "",
                                                   'RESISTANCE_1VR'    : "",
                                                   'RESISTANCE_1VL'    : "",
                                                   'V33_DRIVER_CURRENT': "",
                                                   'V33_DRIVER_TURNON' : "",
                                                   'V33_DRIVER_TURNOFF': "",
                                                   'ROOT_BLOB'         : '',
	                                                'ROOT_FILE'         : '',
	                                                'ROOT_DATE'         : ''},
                                                 'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   },
                        ('2S Service Hybrid'     ,'Electrical'):  {  
                                 'kind_of_part' : '2S Service Hybrid',     
                                    'cond_name' : 'Tracker 2S SEH Test Result',
                                   'table_name' : 'TEST_2SSEH',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : '',
                                                   'FAILURE_CAUSE'     : '',
                                                   'KNOWN_PROBLEMS'    : '',
                                                   'COMPUTER'          : '',
                                                   'LOCAL_DATA_PATH'   : '',
                                                   'START_TIME'        : '',
                                                   'END_TIME'          : '',
                                                   'TEST_CARD'         : '',
                                                   'BACKPLANE_POS'     : '',
                                                   'FC7_MACADDR'       : '', 
                                                   'SOFTWARE_VER'      : '',
                                                   'E_MEASUREMENT'     : '',
                                                   'E_MEASUREMENTQLTY' : "",
                                                   'CIC_OUTTEST'       : '',
                                                   'CIC_OUTBADLINES'   : '',
                                                   'FCMD_TEST'         : "",
                                                   'FCMD_BADLINES'     : "",
                                                   'CLOCK_TEST'        : "",
                                                   'CLOCK_BADLINES'    : "",
                                                   'RESET_TEST'        : "",
                                                   'RESET_BADLINES'    : "",
                                                   'RESET_VOLTAGELVLS' : "",
                                                   'I2C_TEST'          : "",
                                                   'I2C_BADMASTERS'    : "",
                                                   'VTRX_VERSION'      : "",
                                                   'VTRX_TEST'         : "",
                                                   'LPGBT_ADCTEST'     : "",
                                                   'LPGBT_BADADCLINES' : "",
                                                   'LPGBT_ADCMVALUES'  : "",
                                                   'LPGBT_GPIOTEST'    : "",
                                                   'LPGBT_GPIOBADLINES': "",
                                                   'EYE_OPENING'       : "",
                                                   'BIASVOLTAGE_TEST'  : "",
                                                   'LEAKCURRENT_TEST'  : "",
                                                   'TEST_LOAD_SETTINGS': "", 
                                                   'TEST_VLTG_SETTINGS': "", 
                                                   'INPUT_VOLTAGE'     : "",
                                                   'EFFICIENCY_TEST'   : "",
                                                   'VOLTAGES_0LOAD'    : "", 
                                                   'CURRENTS_0LOAD'    : "", 
                                                   'EFFICIENCY_0LOAD'  : "",
                                                   'VOLTAGES_50LOAD'   : "", 
                                                   'CURRENTS_50LOAD'   : "", 
                                                   'EFFICIENCY_50LOAD' : "",
                                                   'VOLTAGES_100LOAD'  : "", 
                                                   'CURRENTS_100LOAD'  : "", 
                                                   'EFFICIENCY_100LOAD': "",
                                                   'VOLTAGES_120LOAD'  : "", 
                                                   'CURRENTS_120LOAD'  : "", 
                                                   'EFFICIENCY_120LOAD': "",
                                                   'TEST_CARD_ERRORS'  : "", 
                                                   'TURNON_VOLTAGE'    : "",
                                                   'TURNOFF_VOLTAGE'   : "",
                                                   'RESISTANCE_1V25R'  : "",
                                                   'RESISTANCE_1V25L'  : "",
                                                   'V33_DRIVER_CURRENT': "",
                                                   'V33_DRIVER_TURNON' : "",
                                                   'V33_DRIVER_TURNOFF': "",
                                                   'ROOT_BLOB'         : '',
	                                                'ROOT_FILE'         : '',
	                                                'ROOT_DATE'         : ''},
                                    'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   }
                        }


   def __init__(self, serial, test_type, data_version='v1', inserter=None):
      """Constructor: it requires the serial number and the type of test (test_type). It also
         accepts the data set version (set to 'v1' as default) and the inserter name which is
         not used by default.

         Mandatory template configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            serial:          identifier of the component whose data belong to;
            barcode:         identifier of the component whose data belong to;
            name_label:      identifier of the component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional template parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     person recording the data (override the CERN user name)
      """

      Hybrid.__init__(self,serial)

      self.h_test = test_type    # stores the type of test

      name = '{}_results'.format(serial)

      configuration = {}
      data_description=self.data_description[(self.h_kind,self.h_test)]
      configuration.update(deepcopy(data_description))
      configuration['serial'] = serial
      configuration['data_version']=data_version
      
      ConditionData.__init__(self, name, configuration, None)

      
   def build_data_block(self,dataset):
      """Builds the data block."""
      data_description = self.retrieve('DBvar_vs_TxtHeader')
      data = etree.SubElement(dataset,"DATA")
      tags = data_description.keys()

      # Check for mandatory tags
      for el in self.retrieve("mandatory"):
         if data_description[el]=='':
            raise MissingParameter(self.__class__.__name__,el)

      for el in data_description.keys():
         if self.debug:
            etree.SubElement(data,f"{el}").text = data_description[el]
         elif data_description[el]!='':
            etree.SubElement(data,f"{el}").text = data_description[el]


   def load_run_metadata(self, run_type=None, run_number=None, run_begin=None, run_end=None, 
                               run_operator=None, run_location=None, run_comment=None ):
      """Loads the run metadata."""
      if run_type!=None: self.update_configuration('run_type',run_type)
      if run_number!=None: self.update_configuration('run_number',run_number)
      if run_begin!=None: self.update_configuration('run_begin',run_begin.strftime('%Y-%m-%d %H:%M:%S'))
      if run_end!=None: self.update_configuration('run_end',run_end.strftime('%Y-%m-%d %H:%M:%S'))
      if run_operator!=None: self.update_configuration('run_operator',run_operator)
      if run_location!=None: self.update_configuration('run_location',run_location)
      if run_comment!=None: self.update_configuration('run_comment',run_comment)


   def load_results(self,hybrid_status=None, test_result_cause=None, test_known_problems=None,
                    electrical_measurements=None, electrical_quality = None,
                    seh_electrical_measurements=None,
                    voltages=None,currents=None,efficiencies=None,
                    noise_measurements=None,
                    cbc_out_test=None,
                    cic_in_test=None, cic_out_test=None, ssa_out_test=None,
                    clk_test=None, reset_test=None, fcmd_test=None, i2cmaster_test=None,
                    vtrx_test=None,lpgbt_adctest=None, lpgbt_gpiotest=None,
                    eye_opening=None,
                    opens=None, shorts=None, resistance=None,
                    test_card_errors=None, current_settings=None, voltage_settings=None,
                    leakcurrent_test=None,biasvoltage_test=None,
                    average_temperature=None, resistance_hv=None,
                    hv_connect_test=None, hv_leakcurr_test=None):
      """Loads the data block."""
      if hybrid_status!=None: self.process_qualification(hybrid_status)
      #if hybrid_status=='Needs retest' :  return   # DO not save result if the hybrid need to be retested.

      if test_result_cause!=None and test_result_cause!="": 
         self.update_data_block('FAILURE_CAUSE',test_result_cause)
      if test_known_problems!=None and test_known_problems!="": 
         self.update_data_block('KNOWN_PROBLEMS',test_known_problems)

      if electrical_measurements!=None and len(electrical_measurements)!=0: 
         self.update_data_block('E_MEASUREMENT',json.dumps(electrical_measurements))
      if electrical_quality!=None and electrical_quality!="": 
         self.update_data_block('E_MEASUREMENTQLTY',electrical_quality)
         
      if test_card_errors!=None:
         self.update_data_block('TEST_CARD_ERRORS',test_card_errors)
      if current_settings!=None and len(current_settings)!=0:
         self.update_data_block('TEST_LOAD_SETTINGS',json.dumps(current_settings))
      if voltage_settings!=None and len(voltage_settings)!=0:
         self.update_data_block('TEST_VLTG_SETTINGS',json.dumps(voltage_settings))
      
      if average_temperature!=None:
         self.update_data_block('TEMP_AVG_DEGC',average_temperature)
      
      if(seh_electrical_measurements!=None):
         try:
            self.update_data_block('INPUT_VOLTAGE',str(seh_electrical_measurements['SEHInputVoltage']))
         except KeyError:  print('SEHInputVoltage not found in seh_electrical_measurements')
         try:
            self.update_data_block('TURNON_VOLTAGE',str(seh_electrical_measurements['SEH_turn_on_voltage']))
         except KeyError:  print('SEH_turn_on_voltage not found in seh_electrical_measurements')
         try:
            self.update_data_block('TURNOFF_VOLTAGE',str(seh_electrical_measurements['SEH_turn_off_voltage']))
         except KeyError:  print('SEH_turn_off_voltage not found in seh_electrical_measurements')
         try:
            self.update_data_block('V33_DRIVER_CURRENT',str(seh_electrical_measurements['Regulator_turn_on_current']))
         except KeyError:  print('Regulator_turn_on_current not found in seh_electrical_measurements')
         try:
            self.update_data_block('V33_DRIVER_TURNON',str(seh_electrical_measurements['Regulator_turn_on_current']))
         except KeyError:  print('Regulator_turn_on_current not found in seh_electrical_measurements')
         try:
            self.update_data_block('V33_DRIVER_TURNOFF',str(seh_electrical_measurements['Regulator_turn_off_current']))
         except KeyError:  print('Regulator_turn_off_current not found in seh_electrical_measurements')

      for load in ('0','50','100','120'):
         if efficiencies !=None:
            try:
               self.update_data_block(f'EFFICIENCY_{load}LOAD',str(efficiencies[load]))
            except KeyError:  print(f'{load} load not found in efficiencies')
         if voltages !=None:
            try:
               self.update_data_block(f'VOLTAGES_{load}LOAD',json.dumps(voltages[load]))
            except KeyError:  print(f'{load} load not found in voltages')
            except Exception as e:  print(e)
         if currents !=None:
            try:
               self.update_data_block(f'CURRENTS_{load}LOAD',json.dumps(currents[load]))
            except KeyError:  print(f'{load} load not found in currents')
            except Exception as e:  print(e)
      
      if resistance!=None:
         for tag in ('1V25R','1V25L','1V25T','1VL','1VR','2V55'):
            try:
               self.update_data_block(f'RESISTANCE_{tag}',str(resistance[tag]))
            except KeyError: pass

      if resistance_hv!=None:
         try:
            self.update_data_block('RESISTANCE',str(resistance_hv['resistance']))
         except KeyError:  print('resistance tag not found resistance_hv')
         try:
            self.process_test('Resistance in range',resistance_hv['resistance_ok'],\
                              'RESISTANCE_OK',[])
         except KeyError:  print('resistance_ok tag not found resistance_hv')

      if noise_measurements!=None:
         try:
            self.update_data_block('PEDESTAL_AVG',str(noise_measurements['pedestal_avg']))
         except KeyError:  print('pedestal_avg not found in noise_measurements')
         try:
            self.update_data_block('PEDESTAL_RMS',str(noise_measurements['pedestal_rms']))
         except KeyError:  print('pedestal_rms not found in noise_measurements')
         try:
            self.update_data_block('NOISE_AVG',str(noise_measurements['noise_avg']))
         except KeyError:  print('noise_avg not found in noise_measurements')
         try:
            self.update_data_block('NOISE_RMS',str(noise_measurements['noise_rms']))
         except KeyError:  print('noise_rms not found in noise_measurements')
      
      if cic_in_test!=None :     self.process_test('cic in',cic_in_test,'CIC_INTEST',('CIC_INBADLINES','bad_lines') )#self.process_cic_in_test(cic_in_test)
      if cic_out_test!=None:     self.process_cic_out_test(cic_out_test)
      if ssa_out_test!=None:     self.process_ssa_out_test(ssa_out_test)
      if cbc_out_test!=None:     self.process_cbc_out_test(cbc_out_test)
      if opens!=None:            self.process_opens(opens)
      if shorts!= None:          self.process_shorts(shorts)
      if clk_test!=None:         self.process_test('clock',clk_test,'CLOCK_TEST',[('CLOCK_BADLINES','bad_lines')] )#self.process_clk_test(clk_test)
      if reset_test!=None:       self.process_test('reset',reset_test,'RESET_TEST',[('RESET_BADLINES','bad_lines'),('RESET_VOLTAGELVLS','levels')] )#self.process_reset_test(reset_test)
      if fcmd_test!=None:        self.process_test('fcmd',fcmd_test,'FCMD_TEST',[('FCMD_BADLINES','bad_lines')] )#self.process_fcmd_test(fcmd_test)
      if i2cmaster_test!=None:   self.process_test('I2Cmaster',i2cmaster_test,'I2C_TEST',[('I2C_BADMASTERS','failing')] )#self.process_i2cmaster_test(i2cmaster_test)
      if vtrx_test!=None:        self.process_test('vtrx',vtrx_test,'VTRX_TEST',[] )#self.process_vtrx_test(vtrx_test)
      if lpgbt_adctest!=None:    self.process_test('LPGBT adc',lpgbt_adctest,'LPGBT_ADCTEST',[('LPGBT_BADADCLINES','tbd'),('LPGBT_ADCMVALUES','tbd')] )#self.process_lpgbt_adctest(lpgbt_adctest)
      if lpgbt_gpiotest!=None:   self.process_test('LPGBT gpio',lpgbt_gpiotest,'LPGBT_GPIOTEST',[('LPGBT_GPIOBADLINES','bad_lines')] )
      if eye_opening!=None:      self.process_test('eye opening',eye_opening,'EYE_OPENING',[] )#self.process_eye_opening(eye_opening)
      if leakcurrent_test!=None: self.process_test('leak current',leakcurrent_test,'LEAKCURRENT_TEST',[])
      if biasvoltage_test!=None: self.process_test('bias voltage',biasvoltage_test,'BIASVOLTAGE_TEST',[])
      if efficiencies!=None:     self.process_test('seh efficiencies',efficiencies,'EFFICIENCY_TEST',[])
      if hv_leakcurr_test!=None: self.process_test('leak current',hv_leakcurr_test,'HV_LEAKCURRENT_OK',[])
      if hv_connect_test!=None:  self.process_test('connectivity',hv_connect_test,'HV_CONNECTIVITY_OK',[])
      if hv_leakcurr_test!=None: 
         self.process_test('leak current',hv_leakcurr_test,'HV_LEAKCURRENT_OK',[])
         self.update_data_block('HV_LEAKCURRENT',str(hv_leakcurr_test['value']))


   def load_test_spec(self, computer=None, data_path=None, start_time=None,
                      stop_time=None, test_card=None, backplane_pos=None,
                      fc7_macaddr=None, software_ver=None):
      """Loads the test specification."""
      if computer!=None: self.update_data_block('COMPUTER',computer) 
      if data_path!=None: self.update_data_block('LOCAL_DATA_PATH',data_path)
      if start_time!=None: self.update_data_block('START_TIME',start_time.strftime('%Y-%m-%d %H:%M:%S'))
      if stop_time!=None: self.update_data_block('END_TIME',stop_time.strftime('%Y-%m-%d %H:%M:%S'))
      if test_card!=None: self.update_data_block('TEST_CARD',test_card)
      if backplane_pos!=None: self.update_data_block('BACKPLANE_POS',json.dumps(backplane_pos)) 
      if fc7_macaddr!=None: self.update_data_block('FC7_MACADDR',fc7_macaddr)
      if software_ver!=None: self.update_data_block('SOFTWARE_VER',software_ver)
      

   def process_qualification(self,qualification):
      """Check QUALIFICATION format."""
      if qualification!='Good' and qualification!='Bad' and qualification!='Needs retest':
         raise BadInputParameter('HybridResultT.process_qualification','QUALIFICATION',qualification)
      self.update_data_block('QUALIFICATION',qualification)


   def process_opens(self,opens):
      """Decode the opens test result and encode it as JSON format."""
      try:
         self.update_data_block('OPENS',str(opens['total_opens']))
      except KeyError:
         print('total_opens key not found in opens dictionary')
         return
      
      if self.h_kind=='PS Front-end Hybrid':
         open_channels = { 'SSA'+k.split('_')[1]:v for (k,v) in opens.items() if k!='total_opens'}
      elif self.h_kind=='2S Front-end Hybrid':
         open_channels = { 'CBC'+k.split('_')[1]:v for (k,v) in opens.items() if k!='total_opens'}
      else:
         open_channels = {}
      if len(open_channels)!=0:
         self.update_data_block('OPEN_CH',json.dumps(open_channels))


   def process_shorts(self,shorts):
      """Decode the opens test result and encode it as JSON format."""
      try:
         self.update_data_block('SHORTS',str(shorts['total_shorts']))
      except KeyError:
         print('total_shorts key not found in shorts dictionary')
         return
      
      if self.h_kind=='PS Front-end Hybrid':
         shorted_channels = { 'SSA'+k.split('_')[1]:v for (k,v) in shorts.items() if k!='total_shorts'}
      elif self.h_kind=='2S Front-end Hybrid':
         shorted_channels = { 'CBC'+k.split('_')[1]:v for (k,v) in shorts.items() if k!='total_shorts'}
      else:
         shorted_channels = {}
      if len(shorted_channels)!=0:
         self.update_data_block('SHORTED_CH',json.dumps(shorted_channels))

   
   def process_test(self,name,data,scol_name,add_inputs):
      """Extracts the test result from the data and fills the internal table."""
      try:
         status = data['STATUS'].upper()
      except KeyError:
         print(f'STATUS key not found in the {name} dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter(f'HybridResultT.process_test {name} data','STATUS',status)
      self.update_data_block(scol_name,status)
      try:
         for el in add_inputs:
            self.update_data_block(el[0],json.dumps(data[el[1]]))
      except:  pass
      
   
   def process_cic_in_test(self,cic_in_test):
      """Decode the CIC IN test results and encode them as JSON format."""
      try:
         status = cic_in_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in cic_in_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_cic_in_test','STATUS',status)
      self.update_data_block('CIC_INTEST',status)
      try:
         self.update_data_block('CIC_INBADLINES',json.dumps(cic_in_test['bad_lines']))
      except:  pass


   def process_cic_out_test(self,cic_out_test):
      """Decode the CIC OUT test results and encode them as JSON format."""
      try:
         status = cic_out_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in cic_out_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_cic_out_test','STATUS',status)
      self.update_data_block('CIC_OUTTEST',status)
      try:
         if self.h_kind == 'PS Read-out Hybrid':
            out = {'hybrid0': cic_out_test['bad_lines_hybrid0'],\
                   'hybrid1': cic_out_test['bad_lines_hybrid1']}
            self.update_data_block('CIC_OUTBADLINES',json.dumps(out))
         else:
            self.update_data_block('CIC_OUTBADLINES',json.dumps(cic_out_test['bad_lines']))
      except:  pass

   def process_clk_test(self,clk_test):
      """Decode the CLK test results and encode them as JSON format."""
      try:
         status = clk_test['STATUS'].upper()
      except:
         print('STATUS key not found in cic_clk_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_clk_test','STATUS',status)
      self.update_data_block('CLOCK_TEST',status)
      try:
         self.update_data_block('CLOCK_BADLINES',json.dumps(clk_test['bad_lines']))
      except:  pass
      
   def process_reset_test(self,reset_test):
      """Decode the RESET test results and encode them as JSON format."""
      try:
         status = reset_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in reset_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_reset_test','STATUS',status)
      self.update_data_block('RESET_TEST',status)
      try:
         self.update_data_block('RESET_BADLINES',json.dumps(reset_test['bad_lines']))
         self.update_data_block('RESET_VOLTAGELVLS',json.dumps(reset_test['levels']))
      except:  pass
      
   def process_fcmd_test(self,fcmd_test):
      """Decode the RESET test results and encode them as JSON format."""
      try:
         status = fcmd_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in fcmd_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_fcmd_test','STATUS',status)
      self.update_data_block('FCMD_TEST',status)
      try:
         self.update_data_block('FCMD_BADLINES',json.dumps(fcmd_test['bad_lines']))
         #self.update_data_block('RESET_VOLTAGELVLS',json.dumps(fcmd_test['levels']))
      except:  pass

   def process_vtrx_test(self,vtrx_test):
      """Decode the VTRX+ test results and encode them as JSON format."""
      try:
         status = vtrx_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in vtrx_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_vtrx_test','STATUS',status)
      self.update_data_block('VTRX_TEST',status)
      
   
   def process_lpgbt_adctest(self,lpgbt_adctest):
      """Decode the LpGBT test results and encode them as JSON format."""
      try:
         status = lpgbt_adctest['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in lpgbt_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_lpgbt_adctest','STATUS',status)
      self.update_data_block('LPGBT_ADCTEST',status)
      

   def process_eye_opening(self,eye_opening):
      """Decode the LpGBT test results and encode them as JSON format."""
      try:
         status = eye_opening['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in eye_opening dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_eye_opening','STATUS',status)
      self.update_data_block('EYE_OPENING',status)
      

   def process_i2cmaster_test(self,i2cmaster_test):
      """Decode the RESET test results and encode them as JSON format."""
      try:
         status = i2cmaster_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in i2cmaster_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_i2cmaster_test','STATUS',status)
      self.update_data_block('I2C_TEST',status)
      try:
         self.update_data_block('I2C_BADMASTERS',json.dumps(i2cmaster_test['failing']))
      except:  pass
      
   def process_ssa_out_test(self,ssa_out_test):
      """Decode the SSA test results and encode as JSON format."""
      try:
         status = ssa_out_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in ssa_out_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_ssa_out_test','STATUS',status)

      self.update_data_block('SSA_OUTTEST',status)
      ssa_with_badlines = sorted( [k for k in ssa_out_test.keys() if 'SSA' in k] )
      
      ssa_bad_stublines = {}
      for ssa in ssa_with_badlines:  
         if 'stub_lines' in ssa_out_test[ssa].keys():
            lines = ssa_out_test[ssa]['stub_lines']
            ssa_bad_stublines[ssa] = [ int(x) for x in lines ]
      if len(ssa_bad_stublines)!=0:
         self.update_data_block('SSA_BAD_STUBLINES',json.dumps(ssa_bad_stublines))

      ssa_bad_l1lines = [ssa for ssa in ssa_with_badlines if 'L1' in ssa_out_test[ssa].keys()]
      if len(ssa_bad_l1lines)!=0:
         self.update_data_block('SSA_BAD_L1LINES', json.dumps(ssa_bad_l1lines))

      if 'ssa_lateral_bad_lines' in ssa_out_test.keys():
         ssa_bad_laterals = [(f'SSA{c[0]}',f'SSA{c[1]}') for c in ssa_out_test['ssa_lateral_bad_lines']]     
         if len(ssa_bad_laterals)!=0:
            self.update_data_block('SSA_BAD_LATERAL', json.dumps(ssa_bad_laterals))


   def process_cbc_out_test(self,cbc_out_test):
      """Decode the CBC test results and encode as JSON format."""
      try:
         status = cbc_out_test['STATUS'].upper()
      except KeyError:
         print('STATUS key not found in cbc_out_test dictionary')
         return
      
      if status=='NOT RUN':  return   # Nothing to do if the test hasn't run
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_cbc_out_test','STATUS',status)

      self.update_data_block('CBC_OUTTEST',status)
      cbc_with_badlines = sorted( [k for k in cbc_out_test.keys() if 'CBC' in k] )
      
      cbc_bad_stublines = {}
      for cbc in cbc_with_badlines:  
         if 'stub_lines' in cbc_out_test[cbc].keys():
            lines = cbc_out_test[cbc]['stub_lines']
            cbc_bad_stublines[cbc] = [ int(x) for x in lines ]
      if len(cbc_bad_stublines)!=0:
         self.update_data_block('CBC_BAD_STUBLINES',json.dumps(cbc_bad_stublines))

      cbc_bad_l1lines = [cbc for cbc in cbc_with_badlines if 'L1' in cbc_out_test[cbc].keys()]
      if len(cbc_bad_l1lines)!=0:
         self.update_data_block('CBC_BAD_L1LINES', json.dumps(cbc_bad_l1lines))

      if 'cbc_lateral_bad_lines' in cbc_out_test.keys():
         cbc_bad_laterals = [(f'CBC{c[0]}',f'CBC{c[1]}') for c in cbc_out_test['cbc_lateral_bad_lines']]     
         if len(cbc_bad_laterals)!=0:
            self.update_data_block('CBC_BAD_LATERAL', json.dumps(cbc_bad_laterals))
            
            

   def load_root_file(self,ph2_working_dir,data_path):
      """Load the root file."""
      if (self.h_kind=='PS Power Hybrid'):  return
      
      if (self.h_kind=='PS Front-end Hybrid' or\
          self.h_kind=='2S Service Hybrid'):   # Merge the two result files supposing they are in the same dir.
         import subprocess, os
         
         files  = search_files(data_path,"*.root")
         target = os.path.join(os.getcwd(),f'{self.serial}_results.root')
         
         if len(files) == 0:
            # Root file not produced by the test, return
            return
         elif len(files) > 1:
            # Multiple root files produced by the test, merge them
            cmd = f'cd {ph2_working_dir}; source ./setup.sh; hadd -f {target} {files[0]} {files[1]}' 
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                                 bufsize=0, close_fds=True, encoding='UTF-8', shell=True)

            out,err = p.communicate()
            print (err)
            if p.returncode!=0:
               raise BadExecution(cmd,err)
         else:
            # Only one root file produced by the test, copy it
            from shutil import copyfile
            import os
            source = os.path.join(data_path,'Hybrid.root')
            copyfile(source,target)
      else:
         from shutil import copyfile
         import os
         source = os.path.join(data_path,'Hybrid.root')
         target = os.path.join(os.getcwd(),f'{self.serial}_results.root')
         copyfile(source,target)
      
      data_block = self.retrieve('DBvar_vs_TxtHeader')
      filename   = f'{self.serial}_results.root'
      filetime   = os.path.getmtime(filename)
      from datetime import datetime
      dt = datetime.fromtimestamp(filetime)
      self.update_data_block('ROOT_FILE',filename)
      self.update_data_block('ROOT_DATE',dt.strftime('%Y-%m-%d %H:%M:%S'))
      #data_block['ROOT_FILE'] = filename
      #data_block['ROOT_DATE'] = dt.strftime('%Y-%m-%d %H:%M:%S')


   #def load_result_file(self,ph2_working_dir,data_path):
   #   """Load the result txt file."""
   #   if (self.h_kind!='PS Power Hybrid'):  return
   #   
   #   from shutil import copyfile
   #   import os
   #   file   = search_files(data_path,"*.txt")[0]
   #   source = os.path.join(data_path, file)
   #   target = os.path.join(os.getcwd(),f'{self.serial}_results.txt')
   #   copyfile(source,target)
      
   #   data_block = self.retrieve('DBvar_vs_TxtHeader')
   #   filename   = f'{self.serial}_results.txt'
   #   filetime   = os.path.getmtime(filename)
   #   from datetime import datetime
   #   dt = datetime.fromtimestamp(filetime)
   #   data_block['RESULT_FILE'] = filename
   #   data_block['RESULT_DATE'] = dt.strftime('%Y-%m-%d %H:%M:%S')


   def update_data_block(self,name,value):
      """Update the data block content. Check the consistency of the name tag."""
      data_block = self.retrieve('DBvar_vs_TxtHeader')
      data_block_tags = data_block.keys()
      if name not in data_block_tags:
         raise BadParameters(self.__class__.__name__,f'{name} is not a tag for {self.h_kind} {self.h_test}.')
      data_block[name]=value
      
      
   def dump_xml_data(self, filename='', tag=''):
      """Dump the sensor condition data in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root)

      if filename == '':
         filename = f'{self.name}.xml'

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      
      from zipfile import ZipFile
      import os
      zipfilename = f'{self.serial}{tag}.zip'
      to_be_zipped = []
      if os.path.isfile(f'{self.serial}_results.root'):
         to_be_zipped = [f'{self.serial}_results.xml', f'{self.serial}_results.root']
      else:
         to_be_zipped = [f'{self.serial}_results.xml']
      
      with ZipFile(zipfilename, 'w') as (zip):
         for f in to_be_zipped: zip.write(f)
         for f in to_be_zipped: os.remove(f)
      
      return zipfilename
