07.06.2019 18:08 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000070/calibration/Calibration_Electron_07-06-19_18h07/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

07.06.2019 18:08 ||I| Creating directory: Results/IntegratedTester_Electron_07-06-19_18h08
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
07.06.2019 18:08 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
07.06.2019 18:08 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
07.06.2019 18:08 ||I| [1m[32mSetting the I2C address table[0m
07.06.2019 18:08 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
07.06.2019 18:08 ||I| Enabling Hybrid 0
07.06.2019 18:08 ||I|      Enabling Chip 0
07.06.2019 18:08 ||I|      Enabling Chip 1
07.06.2019 18:08 ||I|      Enabling Chip 2
07.06.2019 18:08 ||I|      Enabling Chip 3
07.06.2019 18:08 ||I|      Enabling Chip 4
07.06.2019 18:08 ||I|      Enabling Chip 5
07.06.2019 18:08 ||I|      Enabling Chip 6
07.06.2019 18:08 ||I|      Enabling Chip 7
07.06.2019 18:08 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
07.06.2019 18:08 ||I| Successfully received *Pings* from 8 Cbcs
07.06.2019 18:08 ||I| [32mCBC3 Phase tuning finished succesfully[0m
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 6, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| Waiting for DDR3 to finish initial calibration
07.06.2019 18:08 ||I| [32mSuccessfully configured Board 0[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 0[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 1[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 2[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 3[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 4[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 5[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 6[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 7[0m
07.06.2019 18:08 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 399092[0m
07.06.2019 18:08 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464643[0m
07.06.2019 18:08 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464658[0m
07.06.2019 18:08 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464673[0m
07.06.2019 18:08 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464688[0m
07.06.2019 18:08 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464701[0m
07.06.2019 18:08 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464674[0m
07.06.2019 18:08 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 18:08 ||I| [1m[34m CHIP ID FUSE 464659[0m
07.06.2019 18:08 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Created Object Maps and parsed settings:
07.06.2019 18:08 ||I| 	Nevents = 100
07.06.2019 18:08 ||I| 	TestPulseAmplitude = 0
07.06.2019 18:08 ||I|   Target Vcth determined algorithmically for CBC3
07.06.2019 18:08 ||I|   Target Offset fixed to half range (0x80) for CBC3
07.06.2019 18:08 ||I| [1m[34mExtracting Target VCth ...[0m
07.06.2019 18:08 ||I| Disabling all channels by setting offsets to 0xff
07.06.2019 18:08 ||I| Setting offsets of Test Group -1 to 0xff
07.06.2019 18:08 ||I| [32mEnabling Test Group....-1[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group -1 to 0x80
07.06.2019 18:08 ||I| [31mDisabling Test Group....-1[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group -1 to 0xff
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m602[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m598[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m585[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m594[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m589[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m592[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m589[0m
07.06.2019 18:08 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m592[0m
07.06.2019 18:08 ||I| [1m[34mMean VCth value of all chips is 592 - using as TargetVcth value for all chips![0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....0[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 0 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....0[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....1[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 1 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....1[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....2[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 2 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....2[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....3[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 3 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....3[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....4[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 4 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....4[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....5[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 5 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....5[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....6[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 6 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....6[0m
07.06.2019 18:08 ||I| [32mEnabling Test Group....7[0m
07.06.2019 18:08 ||I| Setting offsets of Test Group 7 to 0xff
07.06.2019 18:08 ||I| [31mDisabling Test Group....7[0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 18:08 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
07.06.2019 18:08 ||I| Results saved!
07.06.2019 18:08 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_07-06-19_18h08[0m
07.06.2019 18:08 ||I| Calibration finished.
Calibration of the DUT finished at: Fri Jun  7 18:08:26 2019
	elapsed time: 9.74862 seconds
07.06.2019 18:08 ||I| Starting noise occupancy test.
07.06.2019 18:08 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
07.06.2019 18:08 ||I| [1m[32mSetting the I2C address table[0m
07.06.2019 18:08 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
07.06.2019 18:08 ||I| Enabling Hybrid 0
07.06.2019 18:08 ||I|      Enabling Chip 0
07.06.2019 18:08 ||I|      Enabling Chip 1
07.06.2019 18:08 ||I|      Enabling Chip 2
07.06.2019 18:08 ||I|      Enabling Chip 3
07.06.2019 18:08 ||I|      Enabling Chip 4
07.06.2019 18:08 ||I|      Enabling Chip 5
07.06.2019 18:08 ||I|      Enabling Chip 6
07.06.2019 18:08 ||I|      Enabling Chip 7
07.06.2019 18:08 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
07.06.2019 18:08 ||I| Successfully received *Pings* from 8 Cbcs
07.06.2019 18:08 ||I| [32mCBC3 Phase tuning finished succesfully[0m
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 6, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
07.06.2019 18:08 ||I| 		 Mode: 0
07.06.2019 18:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 18:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 18:08 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 18:08 ||I| Waiting for DDR3 to finish initial calibration
07.06.2019 18:08 ||I| [32mSuccessfully configured Board 0[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 0[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 1[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 2[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 3[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 4[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 5[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 6[0m
07.06.2019 18:08 ||I| [32mSuccessfully configured Cbc 7[0m
07.06.2019 18:08 ||I| Trigger source 01: 7
07.06.2019 18:08 ||I| Histo Map for Module 0 does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
07.06.2019 18:08 ||I| 7
07.06.2019 18:08 ||I| Mesuring Efficiency per Strip ... 
07.06.2019 18:08 ||I| Taking data with 100 Events!
07.06.2019 18:08 ||I| 		Mean occupancy for the Top side: 61.6837 error= 8.77454[0m
07.06.2019 18:08 ||I| 		Mean occupancy for the Botton side: 62.0601 error= 8.34215[0m
# Noisy channels on Bottom Sensor : 2,4,7,9,11,12,16,18,22,23,24,28,31,34,37,39,41,44,46,47,50,52,55,61,65,68,69,72,75,76,79,80,81,84,86,88,91,93,94,95,96,97,100,101,102,103,104,108,110,111,117,118,121,122,126,127,129,132,133,139,140,145,150,151,154,156,158,159,164,166,167,170,171,172,175,179,183,189,191,197,208,209,211,214,215,220,223,225,226,229,230,232,239,242,245,246,247,250,251,255,257,261,262,270,271,272,273,274,280,281,287,294,296,297,301,303,306,309,310,312,313,314,318,322,324,325,327,331,334,336,337,338,346,347,348,349,350,351,352,353,354,356,360,365,369,372,373,377,380,382,386,387,389,392,394,395,397,398,402,403,404,406,408,410,412,413,414,418,420,422,424,425,429,430,431,434,442,443,444,445,446,448,450,451,452,453,455,457,459,460,462,463,464,468,469,471,472,474,475,476,478,481,485,486,491,493,494,495,498,501,502,503,506,507,509,510,512,513,514,516,522,532,535,536,538,539,540,545,550,551,554,557,559,565,567,569,571,577,581,583,585,593,597,598,599,601,602,607,611,616,619,620,621,622,625,628,636,637,639,641,642,644,645,646,648,650,651,652,655,656,658,662,669,674,675,679,680,681,685,687,688,689,691,694,696,697,700,701,703,704,706,709,710,711,712,718,720,722,725,732,735,736,738,743,744,747,751,752,753,755,757,758,760,766,767,769,772,774,775,777,778,781,782,783,787,789,790,792,797,799,800,802,803,805,806,807,810,815,817,818,819,821,823,824,828,837,838,839,840,843,844,846,849,850,852,854,856,857,858,860,863,869,870,872,874,875,876,877,878,879,883,885,886,887,888,889,890,893,900,901,902,904,906,907,908,909,911,912,913,918,920,921,924,926,928,931,938,939,940,944,945,948,949,952,958,961,962,967,969,973,975,976,977,979,980,981,982,984,987,990,995,997,1000,1001,1004,1006,1007,1009,1010,1011,1012,1013,1014
# Noisy channels on Top Sensor : 4,5,8,13,14,18,20,28,30,31,37,38,39,44,45,47,50,51,53,56,57,58,59,68,69,75,76,77,83,85,86,87,89,90,110,111,115,117,118,123,126,130,135,141,144,146,148,162,163,167,168,170,174,179,180,200,212,213,214,217,218,219,223,227,229,230,232,236,239,240,241,242,244,245,248,249,259,262,263,265,267,270,274,275,278,279,280,285,286,291,293,294,295,296,297,298,305,306,309,310,315,317,319,322,326,327,329,330,334,336,337,339,342,346,347,353,356,364,366,367,369,371,372,373,374,375,377,383,384,385,388,393,396,398,399,402,404,406,407,408,413,414,415,416,417,418,419,420,425,426,428,430,431,432,433,435,436,437,439,440,442,445,447,448,450,451,452,455,456,459,461,463,466,467,468,477,479,480,482,483,484,485,486,487,492,494,496,498,501,506,511,512,513,515,517,521,522,524,526,527,537,538,546,549,552,555,556,557,558,559,562,563,565,566,568,570,571,574,576,577,578,580,582,588,592,593,596,597,599,601,602,603,605,610,611,612,613,616,617,620,621,622,625,627,630,631,635,641,645,649,651,654,655,656,659,660,661,662,667,668,669,674,683,684,685,693,694,695,696,698,701,703,704,705,709,713,714,715,719,720,721,722,723,724,725,729,730,733,735,738,739,740,743,745,747,748,749,750,752,755,756,758,759,761,764,766,768,769,773,774,777,779,780,782,784,785,791,793,797,798,800,802,803,804,807,808,810,812,817,818,824,826,827,829,831,834,836,839,841,845,846,847,855,856,858,859,860,867,871,872,873,877,878,879,880,882,883,885,887,888,897,902,903,905,908,909,911,919,921,924,926,927,930,932,936,937,939,942,943,944,945,951,952,955,956,957,959,962,968,972,973,974,977,979,983,985,989,991,997,1002,1004,1009,1012,1013
# Dead channels on Bottom Sensor : 
# Dead channels on Top Sensor : 
07.06.2019 18:08 ||I| Results saved!
(Noise) Occupancy measurement finished at: Fri Jun  7 18:08:28 2019
	elapsed time: 1.37591 seconds
Complete system test finished at: Fri Jun  7 18:08:28 2019
	elapsed time: 12.4855 seconds
07.06.2019 18:08 ||I| [1m[31mclosing result file![0m
07.06.2019 18:08 ||I| [1m[31mDestroying memory objects[0m
User comment: 
