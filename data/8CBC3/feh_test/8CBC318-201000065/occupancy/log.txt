12.06.2019 15:23 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000065/calibration/Calibration_Electron_12-06-19_15h21/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

12.06.2019 15:23 ||I| Creating directory: Results/IntegratedTester_Electron_12-06-19_15h23
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
12.06.2019 15:23 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
12.06.2019 15:23 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 15:23 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 15:23 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 15:23 ||I| Enabling Hybrid 0
12.06.2019 15:23 ||I|      Enabling Chip 0
12.06.2019 15:23 ||I|      Enabling Chip 1
12.06.2019 15:23 ||I|      Enabling Chip 2
12.06.2019 15:23 ||I|      Enabling Chip 3
12.06.2019 15:23 ||I|      Enabling Chip 4
12.06.2019 15:23 ||I|      Enabling Chip 5
12.06.2019 15:23 ||I|      Enabling Chip 6
12.06.2019 15:23 ||I|      Enabling Chip 7
12.06.2019 15:23 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
12.06.2019 15:23 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 15:23 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 1, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 5, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 15:23 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 15:23 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 5859[0m
12.06.2019 15:23 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 5845[0m
12.06.2019 15:23 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 5833[0m
12.06.2019 15:23 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 5822[0m
12.06.2019 15:23 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 0[0m
12.06.2019 15:23 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 0[0m
12.06.2019 15:23 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 5827[0m
12.06.2019 15:23 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 15:23 ||I| [1m[34m CHIP ID FUSE 5834[0m
12.06.2019 15:23 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 15:23 ||I| Created Object Maps and parsed settings:
12.06.2019 15:23 ||I| 	Nevents = 100
12.06.2019 15:23 ||I| 	TestPulseAmplitude = 0
12.06.2019 15:23 ||I|   Target Vcth determined algorithmically for CBC3
12.06.2019 15:23 ||I|   Target Offset fixed to half range (0x80) for CBC3
12.06.2019 15:23 ||I| [1m[34mExtracting Target VCth ...[0m
12.06.2019 15:23 ||I| Disabling all channels by setting offsets to 0xff
12.06.2019 15:23 ||I| Setting offsets of Test Group -1 to 0xff
12.06.2019 15:23 ||I| [32mEnabling Test Group....-1[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group -1 to 0x80
12.06.2019 15:23 ||I| [31mDisabling Test Group....-1[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group -1 to 0xff
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m587[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m605[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m602[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m602[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m594[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m604[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m601[0m
12.06.2019 15:23 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m587[0m
12.06.2019 15:23 ||I| [1m[34mMean VCth value of all chips is 597 - using as TargetVcth value for all chips![0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....0[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 0 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....0[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....1[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 1 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....1[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....2[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 2 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....2[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....3[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 3 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....3[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....4[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 4 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....4[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....5[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 5 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....5[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....6[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 6 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....6[0m
12.06.2019 15:23 ||I| [32mEnabling Test Group....7[0m
12.06.2019 15:23 ||I| Setting offsets of Test Group 7 to 0xff
12.06.2019 15:23 ||I| [31mDisabling Test Group....7[0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 15:23 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
12.06.2019 15:23 ||I| Results saved!
12.06.2019 15:23 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_12-06-19_15h23[0m
12.06.2019 15:23 ||I| Calibration finished.
Calibration of the DUT finished at: Wed Jun 12 15:23:59 2019
	elapsed time: 10.6454 seconds
12.06.2019 15:23 ||I| Starting noise occupancy test.
12.06.2019 15:23 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 15:23 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 15:23 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 15:23 ||I| Enabling Hybrid 0
12.06.2019 15:23 ||I|      Enabling Chip 0
12.06.2019 15:23 ||I|      Enabling Chip 1
12.06.2019 15:23 ||I|      Enabling Chip 2
12.06.2019 15:23 ||I|      Enabling Chip 3
12.06.2019 15:23 ||I|      Enabling Chip 4
12.06.2019 15:23 ||I|      Enabling Chip 5
12.06.2019 15:23 ||I|      Enabling Chip 6
12.06.2019 15:23 ||I|      Enabling Chip 7
12.06.2019 15:23 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
12.06.2019 15:23 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 15:23 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 1, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 5, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 15:23 ||I| 		 Mode: 0
12.06.2019 15:23 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 15:23 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 15:23 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 15:23 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 15:23 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 15:23 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 15:24 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 15:24 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 15:24 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 15:24 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 15:24 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 15:24 ||I| Trigger source 01: 7
12.06.2019 15:24 ||I| Histo Map for Module 0 does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 15:24 ||I| 7
12.06.2019 15:24 ||I| Mesuring Efficiency per Strip ... 
12.06.2019 15:24 ||I| Taking data with 100 Events!
12.06.2019 15:24 ||I| 		Mean occupancy for the Top side: 66.4433 error= 15.0129[0m
12.06.2019 15:24 ||I| 		Mean occupancy for the Botton side: 65.6581 error= 13.6238[0m
# Noisy channels on Bottom Sensor : 15,16,21,26,29,37,38,41,43,45,46,48,50,51,52,60,63,77,78,82,84,85,90,96,99,106,108,109,110,111,112,113,114,116,119,120,124,125,128,132,136,139,140,142,153,158,160,164,168,170,178,184,188,189,195,217,225,227,230,233,243,247,249,251,253,257,260,263,264,265,267,274,279,281,282,293,294,295,298,300,302,311,315,316,317,319,320,328,330,331,333,335,339,341,345,350,354,359,362,364,366,369,372,374,385,391,392,396,407,408,427,434,437,441,443,449,456,462,467,468,470,473,475,477,482,489,493,498,501,504,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,710,711,712,713,714,715,716,717,718,719,720,721,723,724,725,726,727,728,729,730,731,732,733,734,735,736,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,774,787,788,794,798,800,801,804,815,821,823,825,828,832,846,847,849,850,852,853,869,871,883,887,892,893,895,898,903,904,907,911,916,918,920,921,924,929,935,936,939,940,941,943,944,946,948,950,957,964,967,969,971,975,978,983,989,994,996,1000,1004,1005,1006,1007,1008,1009,1012,1013,1015
# Noisy channels on Top Sensor : 1,8,10,11,18,23,25,26,29,30,33,49,55,61,74,80,87,93,94,96,97,99,101,102,106,113,115,116,120,121,125,129,132,133,147,150,156,158,159,161,164,168,169,170,172,180,183,184,190,191,193,198,199,200,201,204,205,207,216,217,219,222,228,229,233,234,239,241,243,246,255,256,257,258,264,269,271,279,282,284,288,297,299,300,306,308,309,311,313,316,319,325,329,331,333,334,335,337,338,342,353,354,355,358,359,360,371,375,378,380,382,383,398,400,403,405,408,410,413,421,423,424,435,436,437,439,441,444,452,460,465,470,476,477,478,479,485,489,492,493,495,505,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,765,771,775,778,792,794,802,806,809,812,816,819,821,823,827,829,830,836,837,843,848,852,855,859,860,863,866,869,878,880,881,884,885,888,890,891,895,899,903,908,910,912,913,914,918,921,925,926,930,935,937,938,948,951,954,957,959,961,962,964,966,972,973,974,975,976,981,984,986,988,990,991,995,1008,1009,1010,1011,1013,1014,1015
# Dead channels on Bottom Sensor : 495
# Dead channels on Top Sensor : 195,240,507
12.06.2019 15:24 ||I| Results saved!
(Noise) Occupancy measurement finished at: Wed Jun 12 15:24:00 2019
	elapsed time: 1.48513 seconds
Complete system test finished at: Wed Jun 12 15:24:00 2019
	elapsed time: 13.4783 seconds
12.06.2019 15:24 ||I| [1m[31mclosing result file![0m
12.06.2019 15:24 ||I| [1m[31mDestroying memory objects[0m
User comment: 
