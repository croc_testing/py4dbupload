12.06.2019 16:08 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000063/calibration/Calibration_Electron_12-06-19_16h07/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

12.06.2019 16:08 ||I| Creating directory: Results/IntegratedTester_Electron_12-06-19_16h08
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
12.06.2019 16:08 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
12.06.2019 16:08 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 16:08 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 16:08 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 16:08 ||I| Enabling Hybrid 0
12.06.2019 16:08 ||I|      Enabling Chip 0
12.06.2019 16:08 ||I|      Enabling Chip 1
12.06.2019 16:08 ||I|      Enabling Chip 2
12.06.2019 16:08 ||I|      Enabling Chip 3
12.06.2019 16:08 ||I|      Enabling Chip 4
12.06.2019 16:08 ||I|      Enabling Chip 5
12.06.2019 16:08 ||I|      Enabling Chip 6
12.06.2019 16:08 ||I|      Enabling Chip 7
12.06.2019 16:08 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 20000040
Reply from chip 1: 20040040
Reply from chip 2: 20080040
Reply from chip 3: 200c0040
Reply from chip 4: 20100040
Reply from chip 5: 20140040
Reply from chip 6: 20180040
Reply from chip 7: 201c0040
12.06.2019 16:08 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 16:08 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 16:08 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 16:08 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5816[0m
12.06.2019 16:08 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5839[0m
12.06.2019 16:08 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5853[0m
12.06.2019 16:08 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5882[0m
12.06.2019 16:08 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5821[0m
12.06.2019 16:08 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5832[0m
12.06.2019 16:08 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5844[0m
12.06.2019 16:08 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 16:08 ||I| [1m[34m CHIP ID FUSE 5858[0m
12.06.2019 16:08 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Created Object Maps and parsed settings:
12.06.2019 16:08 ||I| 	Nevents = 100
12.06.2019 16:08 ||I| 	TestPulseAmplitude = 0
12.06.2019 16:08 ||I|   Target Vcth determined algorithmically for CBC3
12.06.2019 16:08 ||I|   Target Offset fixed to half range (0x80) for CBC3
12.06.2019 16:08 ||I| [1m[34mExtracting Target VCth ...[0m
12.06.2019 16:08 ||I| Disabling all channels by setting offsets to 0xff
12.06.2019 16:08 ||I| Setting offsets of Test Group -1 to 0xff
12.06.2019 16:08 ||I| [32mEnabling Test Group....-1[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group -1 to 0x80
12.06.2019 16:08 ||I| [31mDisabling Test Group....-1[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group -1 to 0xff
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m585[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m601[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m585[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m594[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m594[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m590[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m590[0m
12.06.2019 16:08 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m604[0m
12.06.2019 16:08 ||I| [1m[34mMean VCth value of all chips is 592 - using as TargetVcth value for all chips![0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....0[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 0 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....0[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....1[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 1 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....1[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....2[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 2 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....2[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....3[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 3 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....3[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....4[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 4 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....4[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....5[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 5 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....5[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....6[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 6 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....6[0m
12.06.2019 16:08 ||I| [32mEnabling Test Group....7[0m
12.06.2019 16:08 ||I| Setting offsets of Test Group 7 to 0xff
12.06.2019 16:08 ||I| [31mDisabling Test Group....7[0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 16:08 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
12.06.2019 16:08 ||I| Results saved!
12.06.2019 16:08 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_12-06-19_16h08[0m
12.06.2019 16:08 ||I| Calibration finished.
Calibration of the DUT finished at: Wed Jun 12 16:08:59 2019
	elapsed time: 10.5619 seconds
12.06.2019 16:08 ||I| Starting noise occupancy test.
12.06.2019 16:08 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 16:08 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 16:08 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 16:08 ||I| Enabling Hybrid 0
12.06.2019 16:08 ||I|      Enabling Chip 0
12.06.2019 16:08 ||I|      Enabling Chip 1
12.06.2019 16:08 ||I|      Enabling Chip 2
12.06.2019 16:08 ||I|      Enabling Chip 3
12.06.2019 16:08 ||I|      Enabling Chip 4
12.06.2019 16:08 ||I|      Enabling Chip 5
12.06.2019 16:08 ||I|      Enabling Chip 6
12.06.2019 16:08 ||I|      Enabling Chip 7
12.06.2019 16:08 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 20000040
Reply from chip 1: 20040040
Reply from chip 2: 20080040
Reply from chip 3: 200c0040
Reply from chip 4: 20100040
Reply from chip 5: 20140040
Reply from chip 6: 20180040
Reply from chip 7: 201c0040
12.06.2019 16:08 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 16:08 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 16:09 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 16:09 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 16:09 ||I| Trigger source 01: 7
12.06.2019 16:09 ||I| Histo Map for Module 0 does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 16:09 ||I| 7
12.06.2019 16:09 ||I| Mesuring Efficiency per Strip ... 
12.06.2019 16:09 ||I| Taking data with 100 Events!
12.06.2019 16:09 ||I| 		Mean occupancy for the Top side: 60.7764 error= 8.91129[0m
12.06.2019 16:09 ||I| 		Mean occupancy for the Botton side: 60.9291 error= 8.33326[0m
# Noisy channels on Bottom Sensor : 3,4,8,11,13,15,16,22,24,34,39,41,45,55,56,57,58,59,61,63,70,71,72,73,74,76,82,83,85,88,89,90,92,94,97,98,101,102,107,111,113,114,117,118,121,122,125,126,128,129,130,132,133,135,137,139,140,144,147,151,154,158,166,177,178,179,182,183,191,193,194,195,198,200,201,202,203,216,217,220,221,223,224,226,228,235,237,241,249,250,257,259,264,266,267,269,271,272,275,280,281,284,286,287,290,291,293,295,299,301,302,307,309,312,314,323,324,325,327,329,331,333,335,336,342,348,350,351,352,354,357,359,360,361,366,368,369,371,373,374,375,376,380,390,391,395,398,399,401,402,404,405,406,410,412,414,415,418,425,427,432,435,440,441,442,444,450,453,458,460,462,463,464,465,466,468,471,473,477,478,481,483,486,489,490,492,494,499,500,506,507,510,514,518,523,525,527,529,531,533,543,544,547,550,552,576,577,580,581,583,585,588,589,590,593,594,595,596,598,599,602,603,608,609,610,611,616,617,619,621,622,630,631,632,635,642,643,644,646,647,651,654,655,656,661,662,664,666,668,669,671,672,674,676,684,685,686,688,689,693,698,702,704,708,709,710,711,713,714,717,719,721,727,728,729,731,733,741,746,747,749,750,751,756,757,760,762,763,771,777,778,779,780,783,796,797,802,804,808,811,818,820,831,832,835,837,842,843,844,845,846,847,851,860,862,863,870,872,877,878,880,881,882,883,888,891,896,901,903,907,913,922,923,924,928,931,932,935,937,940,952,959,961,965,967,975,982,983,984,986,999,1002,1004
# Noisy channels on Top Sensor : 3,5,8,9,10,11,12,13,19,22,24,25,30,38,39,42,45,50,51,54,55,56,57,62,63,64,73,76,79,80,82,85,87,97,102,103,106,108,109,110,111,117,118,121,124,126,127,128,135,136,143,145,153,158,163,169,173,174,176,182,193,212,214,220,224,226,227,229,239,240,246,249,251,253,255,256,257,261,263,264,267,269,270,276,278,281,283,287,292,293,297,299,300,303,304,308,310,312,318,319,321,323,324,326,327,328,330,335,337,339,340,341,343,344,345,347,348,349,350,351,355,359,363,364,368,370,371,376,378,379,381,384,385,391,393,395,400,407,410,411,416,419,421,422,423,424,426,428,429,430,435,436,439,441,443,444,446,448,450,451,452,453,454,455,457,461,462,464,465,469,479,481,484,485,486,488,490,492,493,495,496,499,500,505,509,510,513,514,515,517,521,522,525,528,529,533,537,539,541,555,557,559,564,573,575,578,579,580,581,582,583,589,591,598,604,607,609,610,611,616,617,618,619,621,623,625,627,628,629,630,631,634,635,642,643,646,651,654,659,664,668,669,670,671,674,676,677,680,682,683,685,687,689,692,693,694,698,703,704,705,706,707,708,711,712,716,717,719,721,722,723,724,727,729,730,731,732,737,739,744,745,746,748,751,754,756,757,760,761,762,764,770,774,779,782,784,788,790,793,796,800,802,805,808,811,813,818,820,823,824,826,830,831,833,834,835,837,838,840,844,847,848,849,856,860,861,863,868,869,871,875,876,884,887,888,889,890,891,893,899,903,909,923,924,942,945,954,959,960,968,970,977,980,983,985,994,995,996,1000
# Dead channels on Bottom Sensor : 
# Dead channels on Top Sensor : 566,905
12.06.2019 16:09 ||I| Results saved!
(Noise) Occupancy measurement finished at: Wed Jun 12 16:09:01 2019
	elapsed time: 1.49412 seconds
Complete system test finished at: Wed Jun 12 16:09:01 2019
	elapsed time: 13.41 seconds
12.06.2019 16:09 ||I| [1m[31mclosing result file![0m
12.06.2019 16:09 ||I| [1m[31mDestroying memory objects[0m
User comment: 
